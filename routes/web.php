<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::post('/contact_us', 'ContactUsController@send')->name('contact_us');
Route::get('/product/{slug}', 'HomeController@products')->name('product');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/my/profile', 'UserController@profile')->name('my.profile');
    Route::post('/my/profile', 'UserController@update')->name('my.profile.update');
    Route::get('/my/wallet', 'WalletController@index')->name('my.wallet');
    Route::get('/my/orders', 'OrderController@index')->name('my.orders');
    Route::get('/fund-wallet', 'WalletController@fund')->name('fund.wallet');
    Route::post('/fund-wallet', 'WalletController@storeTransaction')->name('fund.wallet.create');
    Route::post('/order', 'OrderController@add')->name('order.create');
    Route::patch('/transaction/status', 'TransactionController@toggleStatus')->name('transaction.update.status');
});


Route::group(['namespace' => 'Auth'], function () {
    Route::get('/register', 'RegistrationController@showForm')->name('register')->middleware('guest');
    Route::post('/register', 'RegistrationController@register')->name('register.submit')->middleware('guest');

    Route::get('/login', 'LoginController@showForm')->name('login');
    Route::get('/logout', 'LoginController@logout')->name('logout');
    Route::post('/login', 'LoginController@authenticate')->name('login.submit');

    Route::post('/change-password', 'PasswordController@change')->name('change.password')->middleware('auth');
    Route::get('/forgot-password', 'PasswordController@showForm')->name('forgot.password')->middleware('guest');
    Route::post('/forgot-password', 'PasswordController@requestReset')->name('password.request.reset')->middleware('guest');
    Route::get('/password/reset/{token}', 'PasswordController@resetForm')->middleware('guest');
    Route::post('/password/reset', 'PasswordController@resetPassword')->name('password.reset')->middleware('guest');
});

Route::get('/{page}', 'PageController@index');

Route::post('/monnify/webhook', 'WalletController@monnifyWebhook');

