<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

$router = app(Router::class);

$router->group(['prefix'=>'v1', 'namespace' => 'Api\\V1'], function (Router $router) {

    $router->group(['namespace' => 'Admin'], function() use ($router) { // Add auth middleware
        $router->group(['prefix'=>'auth', 'namespace' => 'Auth'], function() use ($router) {
            $router->post('login', 'LoginController@login');
        });

        $router->group(['middleware' => 'auth:api'], function () use ($router) {
            // Users
            $router->group(['prefix'=>'users', 'namespace' => 'Users'], function() use ($router) {
                $router->get('', 'UserController@list');
                $router->get('/search', 'UserController@search');
                $router->get('/{uuid}', 'UserController@details');
                $router->patch('/status/{uuid}', 'UserController@toggleStatus');
            });

            $router->group(['prefix'=>'categories', 'namespace' => 'Categories'], function() use ($router) {
                $router->get('', 'CategoryController@list');
                $router->get('/search', 'CategoryController@search');
                $router->get('/{uuid}', 'CategoryController@details');
                $router->delete('/{uuid}', 'CategoryController@delete');
                $router->post('', 'CategoryController@create');
                $router->post('/update/{uuid}', 'CategoryController@update');
            });

            $router->group(['prefix'=>'products', 'namespace' => 'Products'], function() use ($router) {
                $router->get('', 'ProductController@index');
                $router->get('/search', 'ProductController@search');
                $router->post('', 'ProductController@add');
                $router->get('/{uuid}', 'ProductController@details');
                $router->delete('/{uuid}', 'ProductController@delete');
                $router->delete('/variations/{uuid}', 'ProductVariationController@delete');
                $router->post('/variations', 'ProductVariationController@add');
                $router->patch('/variations/{uuid}', 'ProductVariationController@update');
            });

            $router->group(['prefix'=>'orders', 'namespace' => 'Orders'], function() use ($router) {
                $router->get('', 'OrderController@index');
                $router->get('/search', 'OrderController@search');
                $router->get('/{uuid}', 'OrderController@details');
                $router->patch('/update/status/{uuid}', 'OrderController@changeStatus');
                $router->get('/test/event', 'OrderController@test');
            });

            $router->group(['prefix'=>'transactions', 'namespace' => 'Transactions'], function() use ($router) {
                $router->get('', 'TransactionController@index');
                $router->get('/search', 'TransactionController@search');
                $router->post('', 'TransactionController@add');
//            $router->patch('/update/status/{uuid}', 'TransactionController@changeStatus');
            });

            $router->group(['prefix'=>'stats', 'namespace' => 'Stats'], function() use ($router) {
                $router->get('', 'StatsController@index');
            });

            $router->group(['prefix'=>'announcements', 'namespace' => 'Announcements'], function () use ($router) {
                $router->get('', 'AnnouncementController@list');
                $router->post('', 'AnnouncementController@add');
                $router->get('/{uuid}', 'AnnouncementController@details');
                $router->delete('/{uuid}', 'AnnouncementController@delete');
                $router->patch('/toggle/{uuid}', 'AnnouncementController@toggle');
                $router->patch('/{uuid}', 'AnnouncementController@update');
            });
        });

    });

});
