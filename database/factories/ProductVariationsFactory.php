<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\ProductVariation;
use Faker\Generator as Faker;

$factory->define(ProductVariation::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => $faker->numberBetween(1000, 10000),
        'agent_price' => $faker->numberBetween(1000, 10000),
        'duration' => $faker->word,
        'product_id' => $faker->numberBetween(1,10)
    ];
});
