<?php

use App\User;
use App\UserType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'EXCEL',
            'last_name' => 'GLOBAL',
            'email' => 'superadmin@exceltelecom.org',
            'password' => Hash::make('GaU:\p9}Mf'),
            'phone' => '081694487240',
            'user_type_id' => UserType::TYPE_ADMIN,
            'status' => User::STATUS_ACTIVE
        ]);
    }
}
