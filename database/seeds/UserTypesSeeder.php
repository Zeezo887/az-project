<?php

use App\UserType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('user_types')

        $types = ['admin', 'agent', 'customer'];

        array_walk($types, function ($type) {
            UserType::create([
                'type' => $type
            ]);
        });
    }
}
