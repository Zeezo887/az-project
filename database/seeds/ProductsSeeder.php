<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = ['MTN', 'Glo', '9mobile', 'Airtel'];

        array_walk($products, function ($product) {
            Product::create([
                'name' => $product,
                'category_id' => 1
            ]);
        });
    }
}
