<?php

use App\ProductVariation;
use Illuminate\Database\Seeder;

class ProductVariationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            5 => [
                [
                    'name' => '2gb',
                    'price' => 1000,
                    'agent_price' => 820,
                    'duration' => '1 Month'
                ],
                [
                    'name' => '5gb',
                    'price' => 3000,
                    'agent_price' => 2520,
                    'duration' => '1 Month'
                ],
                [
                    'name' => '11gb',
                    'price' => 4000,
                    'agent_price' => 3500,
                    'duration' => '1 Month'
                ]
            ],
            6 => [
                [
                    'name' => '2gb',
                    'price' => 1000,
                    'agent_price' => 820,
                    'duration' => '1 Month'
                ],
                [
                    'name' => '5gb',
                    'price' => 3000,
                    'agent_price' => 2520,
                    'duration' => '1 Month'
                ],
                [
                    'name' => '11gb',
                    'price' => 4000,
                    'agent_price' => 3500,
                    'duration' => '1 Month'
                ]
            ],
            7 => [
                [
                    'name' => '2gb',
                    'price' => 1000,
                    'agent_price' => 820,
                    'duration' => '1 Month'
                ],
                [
                    'name' => '5gb',
                    'price' => 3000,
                    'agent_price' => 2520,
                    'duration' => '1 Month'
                ],
                [
                    'name' => '11gb',
                    'price' => 4000,
                    'agent_price' => 3500,
                    'duration' => '1 Month'
                ],
                [
                    'name' => '11gb',
                    'price' => 4000,
                    'agent_price' => 3500,
                    'duration' => '1 Month'
                ],
                [
                    'name' => '11gb',
                    'price' => 4000,
                    'agent_price' => 3500,
                    'duration' => '1 Month'
                ]
            ]
        ];

        array_walk($products, function ($variations, $productId) {
            array_walk($variations, function ($variation) use ($productId) {
                ProductVariation::create([
                    'name' => $variation['name'],
                    'price' => $variation['price'],
                    'agent_price' => $variation['agent_price'],
                    'duration' => $variation['duration'],
                    'product_id' => $productId
                ]);
            });
        });
    }
}
