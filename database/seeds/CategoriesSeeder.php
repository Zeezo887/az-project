<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Data Plans', 'TV Subscriptions', 'VTU'];

        array_walk($categories, function ($cat) {
            Category::create(['name' => $cat]);
        });

    }
}
