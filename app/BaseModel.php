<?php
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class BaseModel
 * @package App
 */
class BaseModel extends Model
{
    /**
     * Boot
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->uuid = (string)static::generateUUID();
        });
    }

    /**
     * Generate unique uuid
     * @return \Ramsey\Uuid\UuidInterface
     */
    private static function generateUUID()
    {
        $uuid = Str::orderedUuid();
        if (static::where('uuid', $uuid)->count() > 0) return static::generateUUID();
        return $uuid;
    }
}
