<?php

namespace App;

class Bank extends BaseModel
{
    protected $fillable = [
        'bank_name',
        'account_name',
        'account_no'
    ];

    public $timestamps = false;
}
