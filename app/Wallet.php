<?php
declare(strict_types=1);

namespace App;

/**
 * Class Wallet
 * @package App
 */
class Wallet extends BaseModel
{
    protected $fillable = [
        'user_id',
        'current_balance',
        'earned',
        'spent'
    ];
}
