<?php

namespace App;

use App\Http\Traits\BankTraits;
use App\Notifications\AccountApproved;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;

class User extends BaseAuthenticableModel
{
    use Notifiable, BankTraits;

    const STATUS_PENDING = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCKED = 9;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'phone',
        'user_type_id',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function (Model $model) {
            // Create User Wallet
            Wallet::create(['user_id' => $model->id]);

            try {
                static::createBank($model);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
            }
        });

        static::updated(function (Model $model) {

            // if account was approved
            if ($model->status == self::STATUS_ACTIVE && $model->getOriginal('status') == self::STATUS_PENDING)
                $model->notify(new AccountApproved($model));
        });
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class, 'user_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    public function isAgent()
    {
        return $this->user_type_id == UserType::TYPE_AGENT;
    }

    public function token()
    {
        return $this->hasOne(ResetToken::class, 'user_id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return HasOne
     */
    public function bank(): HasOne
    {
        return $this->hasOne(Bank::class, 'user_id');
    }

    public function hasBank()
    {
        return $this->bank()->count() > 0;
    }
}
