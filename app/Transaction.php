<?php
declare(strict_types=1);

namespace App;

use App\Notifications\WalletCredited;

/**
 * Class Transaction
 * @package App
 */
class Transaction extends BaseModel
{
    const TYPE_CREDIT = 1;
    const TYPE_DEBIT = 2;

    const type = [
        1 => 'Credit',
        2 => 'Debit'
    ];

    const STATUS_PENDING = 0;
    const STATUS_COMPLETED = 1;

    const status = [
        0 => 'Pending',
        1 => 'Completed'
    ];

    protected $fillable = [
//        'reference',
        'amount',
        'user_id',
        'status',
        'type',
        'monnify_reference'
    ];

    public function getStatusReadableAttribute()
    {
        return self::status[$this->status];
    }

    public function getTypeReadableAttribute()
    {
        return self::type[$this->type];
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->reference = static::generateReference();
        });

        static::created(function ($model) {
            if ($model->status == self::STATUS_COMPLETED)
                static::updateWalletBalance($model);

        });

        static::updated(function ($model) {
            if ($model->status == self::STATUS_COMPLETED)
                static::updateWalletBalance($model);
        });
    }

    /**
     * Generate a unique transaction reference
     * @return string
     */
    protected static function generateReference(): string
    {
        $ref = bin2hex(openssl_random_pseudo_bytes(3)) . substr((string)time(), -4);

        if (static::where('reference', $ref)->count() > 0) return static::generateReference();

        return $ref;
    }

    protected static function updateWalletBalance(Transaction $transaction)
    {
        $wallet = Wallet::where('user_id', $transaction->user_id)->first();

        if ($transaction->type == self::TYPE_CREDIT) {
            $wallet->update([
                'current_balance' => $transaction->amount + $wallet->current_balance,
                'earned' => $transaction->amount + $wallet->earned
            ]);

            $transaction->user->notify(new WalletCredited($transaction));
        }
        else
            $wallet->update([
                'current_balance' => $wallet->current_balance - $transaction->amount,
                'spent' => $transaction->amount + $wallet->spent
            ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
