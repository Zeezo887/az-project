<?php
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Category
 * @package App
 */
class Category extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
//        'slug',
        'image',
        'description',
        'active',
        'featured'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'featured' => 'boolean'
    ];

    /**
     * Boot
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->slug = Str::slug($model->name);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }
}
