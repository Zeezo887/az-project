<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsLogs extends Model
{
    protected $fillable = ['phone', 'log'];
}
