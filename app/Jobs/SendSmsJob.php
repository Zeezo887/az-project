<?php

namespace App\Jobs;

use App\SmsLogs;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Unirest\Request;

class SendSmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $message;

    private $phone;

    /**
     * Create a new job instance.
     *
     * @param string $message
     * @param string $phone
     */
    public function __construct(string $message, string $phone)
    {
        $this->message = $message;
        $this->phone = $phone;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $message = urlencode($this->message);
        $sender = urlencode('EXCEL');
        $to = $this->phone;
        $token = 'UztPIPqxJUS7EgQTVWTmjBZsPldkMYRYK2rINlovPwO2dq6cMiOKeIw2UU4waUUBAUvHp8cDWOIK7APEKNxBx4eCpLBf6cD7Fb7K';
        $routing = 2; //basic route = 2
        $type = 0;

        $this->sender($message, $sender, $to, $token, $routing, $type);

        if ($this->phone == '07012253415') {
            $query = [
                'to' => $this->phone,
                'from' => 'EXCELGLOBAL',
                'message' => $this->message
            ];

            $query2 = [
                'to' => '07012253415',
                'from' => 'EXCELGLOBAL',
                'message' => 'Your Jusibe wallet is low. Recharge'
            ];

            Request::auth('2477956b76ba2ec93af995c082f4516a', '4942430c7578e9ef3881daa6a044cd34');

            // Check available credits
            $res = Request::get('https://jusibe.com/smsapi/get_credits');
            if (($credit = $res->body->sms_credits) == 0) return;

            if (($credit = $res->body->sms_credits) == 2) {
                Request::post('https://jusibe.com/smsapi/send_sms', [], $query2);
            }

            Request::post('https://jusibe.com/smsapi/send_sms', [], $query);
        }
    }

    private function sender($message, $sender, $to, $token, $routing, $type)
    {
        $url = "https://smartsmssolutions.com/api/json.php?sender=%s&to=%s&message=%s&type=%s&token=%s&routing=%s";

        $url = sprintf($url, $sender, $to, $message, $type, $token, $routing);

        $res = Request::get($url);

        SmsLogs::create(['phone' => $this->phone, 'log' => json_encode($res->body)]);
    }
}
