<?php
declare(strict_types=1);

namespace App\Http\Services;

use App\User;
use Unirest\Request;

/**
 * Class Monnify
 * @package App\Http\Services
 * @author Olayinka Azeez <obazeez0574@gmail.com>
 */
class Monnify
{
    private $token;

    CONST BASE_URL = 'https://api.monnify.com';

    public function __construct()
    {
        $this->login();
    }

    private function login()
    {
        $key = base64_encode('MK_PROD_GGMAH4L26Y:G4BUR9NFKCUX5YTVM86A9ZK2X44LBA69');
        $res = Request::post(
            self::BASE_URL . '/api/v1/auth/login/',
            [
                'Authorization' => "Basic $key"
            ]
        );

        $this->token = $res->body->responseBody->accessToken;
    }

    /**
     * @param User $user
     * @return mixed
     * @throws \Exception
     */
    public function createAccount(User $user)
    {
        $response = Request::post(
            self::BASE_URL . '/api/v2/bank-transfer/reserved-accounts',
            $this->getHeaders([
                'Content-Type' => 'application/json'
            ]), json_encode([
                "accountReference" => $user->uuid,
                "accountName" => $user->full_name,
                "currencyCode" => "NGN",
                "contractCode" => "676247193651",
                "customerEmail" => $user->email,
                "customerName" => $user->full_name,
                "getAllAvailableBanks" => false,
                "preferredBanks" => ["035"]
            ])
        );

        if ($response->code >= 200 && $response->code < 300) {
            return $response->body->responseBody;
        } else {
            throw new \Exception(' an error occurred: ' . $response->body->responseMessage, $response->code);
        }
    }

    /**
     * @param array $additional_headers
     * @return array|string[]
     */
    private function getHeaders(array $additional_headers = []): array
    {
        return [
            'Authorization' => "Bearer {$this->token}"
        ] + $additional_headers;
    }
}
