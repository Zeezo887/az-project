<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uuid' => $this->uuid,
            'name' => $this->name,
            'image' => $this->image,
            'description' => $this->description,
            'active' => $this->active,
            'created_at' => $this->created_at->format('M d, Y H:i A'),
            'product_count' => $this->products()->count()
        ];
    }
}
