<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AnnouncementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'text' => $this->text,
            'created_at' => $this->created_at->format('M d, Y'),
            'for' => $this->for,
            'for_formatted' => $this->for === 'all' ? 'All' : 'Agents',
            'active' => $this->active
        ];
    }
}
