<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uuid' => $this->uuid,
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'active' => $this->active,
            'has_variant' => $this->has_variant,
            'category_name' => $this->category->name,
            'variant_count' => $this->variants()->count(),
            'created_at' => $this->created_at->format('M d, Y H:i A'),
            'variants' => ProductVariationResource::collection($this->whenLoaded('variants'))
        ];
    }
}
