<?php

namespace App\Http\Resources;

use App\UserType;
use App\Wallet;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    const status = [
        0 => 'Pending',
        1 => 'Active',
        9 => 'Blocked'
    ];
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'display_name' => "$this->first_name $this->last_name",
            'email' => $this->email,
            'phone' => $this->phone,
            'created_at' => $this->created_at->format('M d Y H:i'),
            'status' => $this->status,
            'status_readable' => self::status[$this->status],
            'user_type_id' => $this->user_type_id,
            'user_type' => UserType::types[$this->user_type_id],
            'wallet_balance' => $this->wallet->current_balance
        ];
    }
}
