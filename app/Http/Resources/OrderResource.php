<?php

namespace App\Http\Resources;

use App\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $variant_name = $this->variant->name ?? '';
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'user' => "{$this->user->first_name} {$this->user->last_name}",
            'product' => $product = $this->product->name ?? '',
            'category' => $category = $this->product ? Category::find($this->product->category_id)->name : '',
            'phone' => $this->phone,
            'variant' => $this->variant,
            'status' => $this->status,
            'status_readable' => $this->status_readable,
            'amount' => $this->amount,
            'item' => "$category ($product) $variant_name",
            'created_at' => $this->created_at->format('M d, Y H:i A'),
            'order_number' => "#$this->order_number"
        ];
    }
}
