<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uuid' => $this->uuid,
            'reference' => $this->reference,
            'amount' => $this->amount,
            'user' => "{$this->user->first_name} {$this->user->last_name}",
            'status' => $this->status,
            'status_readable' => $this->status_readable,
            'type' => $this->type,
            'type_readable' => $this->type_readable,
            'created_at' => $this->created_at->format('M d, Y H:i A'),
        ];
    }
}
