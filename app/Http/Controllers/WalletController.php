<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Traits\BankTraits;
use App\Repositories\TransactionRepository;
use App\Transaction;
use App\User;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class WalletController
 * @package App\Http\Controllers
 */
class WalletController extends Controller
{
    use BankTraits;
    /**
     * @var TransactionRepository
     */
    private $transactionRepo;

    const MONNIFY_SUCCESSFUL_TRANSCTION = 'SUCCESSFUL_TRANSACTION';

    /**
     * WalletController constructor.
     * @param TransactionRepository $trans
     */
    public function __construct(TransactionRepository $trans)
    {
        $this->transactionRepo = $trans;
    }

    public function index()
    {
        $wallet = Wallet::where('user_id', auth()->user()->id)->first();

        $transactions = Transaction::where('user_id', auth()->user()->id)->latest()->take(10)->get();

        return view('wallet.index', compact('wallet', 'transactions'));
    }

    public function fund()
    {
        if (! ($user = auth()->user())->hasBank()) {
            try {
                static::createBank($user);
            } catch (\Exception $e) {
                Log::error($e->getMessage());
            }
        }

        $bank = $user->bank;
        return view('wallet.fund', compact('bank'));
    }

    public function storeTransaction(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required'
        ]);

        $data = [
            'amount' => $request->input('amount'),
            'user_id' => $request->user()->id,
            'status' => Transaction::STATUS_PENDING,
            'type' => Transaction::TYPE_CREDIT
        ];

        $tranx = $this->transactionRepo->create($data);

        return response()->json([
            'message' => 'Transaction saved successfully',
            'data' => $tranx
        ]);
    }

    public function monnifyWebhook(Request $request)
    {
//        $secret = 'G4BUR9NFKCUX5YTVM86A9ZK2X44LBA69';
//        $hash = $this->computeSHA512TransactionHash(json_encode($request->json()->all()), $secret);
//
//        if ($hash !== $request->header('monnify-signature')) {
//            Log::error('Hash error');
//            Log::error($hash);
//            Log::error($request->header('monnify-signature'));
//            return;
//        }
//
//
//        Log::error(json_encode($request->json()->all()));

        if ($request->json('eventType') === self::MONNIFY_SUCCESSFUL_TRANSCTION) {
            $eventData = (object)$request->json('eventData');

            $data = [
                'amount' => $eventData->amountPaid,
                'user_id' => User::where('email', $eventData->customer['email'])->first()->id,
                'status' => Transaction::STATUS_COMPLETED,
                'type' => Transaction::TYPE_CREDIT,
                'monnify_reference' => $eventData->transactionReference
            ];

            $this->transactionRepo->create($data);
        }
    }

    private function computeSHA512TransactionHash($stringifiedData, $clientSecret)
    {
        $computedHash = hash_hmac('sha512', $stringifiedData, $clientSecret);
        return $computedHash;
    }
}
