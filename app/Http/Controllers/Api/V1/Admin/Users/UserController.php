<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Admin\Users;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use App\UserType;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function list(Request $request)
    {
        $perPage = $request->get('perPage', 30);

        $users = User::whereIn('user_type_id', [UserType::TYPE_AGENT, UserType::TYPE_CUSTOMER])
            ->latest()->paginate($perPage);

        return UserResource::collection($users);
    }

    /**
     * @param Request $request
     * @param string $uuid
     * @return UserResource|\Illuminate\Http\JsonResponse|mixed
     */
    public function details(Request $request, string $uuid)
    {
        $user = User::where('uuid', $uuid)->first();

        if (!$user)
            return $this->respondWithError('User not found', 404);

        return $this->respondWithSuccess([
            'data' => new UserResource($user)
        ]);
    }

    /**
     * Toggle user status
     * @param Request $request
     * @param string $uuid
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function toggleStatus(Request $request, string $uuid)
    {
        $user = User::where('uuid', $uuid)->first();

        if (!$user)
            return $this->respondWithError('User not found', 404);

        $this->validate($request, [
            'status' => 'required|in:1,9'
        ]);

        if (!$user->update(['status' => $request->input('status')]))
            return $this->respondWithError('Status Update Failed', 500);

        return $this->respondWithSuccess([
            'message' => 'Status updated successfully',
            'data' => new UserResource($user)
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(Request $request)
    {
        $q = rawurldecode($request->get('q', ''));
        $perPage =  $request->get('perPage', 12);

        if (strlen($q) < 3)
            return $this->respondWithError('Search term should be more than 3 characters', 400);

        $query = User::latest()
            ->whereIn('user_type_id', [UserType::TYPE_AGENT, UserType::TYPE_CUSTOMER])
            ->where('first_name', 'like', "%$q%")
            ->orWhere('last_name', 'like', "%$q%")
            ->orWhere('email', 'like', "%$q%")
            ->orWhere('phone', 'like', "$q%");

        return UserResource::collection($query->paginate($perPage));
    }
}
