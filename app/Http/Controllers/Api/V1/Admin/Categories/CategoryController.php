<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Admin\Categories;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Traits\ImageUploaderTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Throwable;

class CategoryController extends Controller
{
    use ImageUploaderTrait;

    private $catImagePath = 'images/categories';

    public function list(Request $request)
    {
        $perPage = $request->get('perPage', 12);

        $categories = Category::latest()->paginate($perPage);

        return CategoryResource::collection($categories);
    }

    public function details($uuid)
    {
        $category = Category::where('uuid', $uuid)->first();

        if (!$category)
            return $this->respondWithError('Category not found', 404);

        return $this->respondWithSuccess([
            'data' => new CategoryResource($category)
        ]);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories,name',
            'image' => 'nullable|image',
            'description' => 'nullable'
        ]);

        $name = $request->input('name');
        $image = [];

        if ($request->file('image')) {
            try {
                $image = $this->uploadCategoryImage($request, $name);
            } catch (Throwable $t) {
                return $this->respondWithError('Image Upload Failed', 500);
            }
        }

        $data = [
            'name' => $name,
            'image' => $image['url'] ?? '',
            'description' => $request->input('description'),
            'active' => false
        ];

        if (!$cat = Category::create($data))
            return $this->respondWithError('Category creation failed', 500);

        return $this->jsonResponse([
            'message' => 'Category created successfully',
            'data' => new CategoryResource($cat)
        ], 201);
    }

    private function uploadCategoryImage(Request $request, $name)
    {
        return static::uploadFormMedia(
            $this->catImagePath,
            Str::slug($name . " " . (string)Str::uuid() . " " . bin2hex(random_bytes(6))),
            $request->file('image')
        );
    }

    public function update(Request $request, string $uuid)
    {
        $cat = Category::where('uuid', $uuid)->first();
        if (!$cat)
            return $this->respondWithError('Category not found', 404);

        $this->validate($request, [
            'name' => 'required|unique:categories,name,' . $cat->id,
            'image' => 'nullable|image',
            'description' => 'nullable'
        ]);

        $cat->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'active' => $request->input('status', $cat->active)
        ]);

        return $this->respondWithSuccess([
            'message' => 'Category updated successfully',
            'data' => new CategoryResource($cat)
        ]);
    }

    /**
     * Search Categories
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(Request $request)
    {
        $q = rawurldecode($request->get('q', ''));
        $perPage = $request->get('perPage', 12);

        if (strlen($q) < 3)
            return $this->respondWithError('Search term should be more than 3 characters', 400);

        $query = Category::latest()->where('name', 'like', "%$q%");

        return CategoryResource::collection($query->paginate($perPage));
    }

    public function delete($uuid)
    {
        $category = Category::where('uuid', $uuid)->first();

        if (!$category)
            return $this->respondWithError('Category not found', 404);

        $category->delete();

        return $this->respondWithSuccess('Category deleted successfully');
    }
}
