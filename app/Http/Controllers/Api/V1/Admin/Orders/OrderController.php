<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Admin\Orders;

use App\Events\NewOrder;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Jobs\SendSmsJob;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class OrderController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $perPage = $request->get('perPage', 30);

        $orders = Order::latest()->paginate($perPage);

        return OrderResource::collection($orders);
    }

    public function details(string $uuid)
    {
        $order = Order::where('uuid', $uuid)->first();

        if (!$order)
            return $this->respondWithError('Order not found', 404);

        return $this->respondWithSuccess([
           'data' => new OrderResource($order)
        ]);
    }

    public function changeStatus(Request $request, string $uuid)
    {
        $order = Order::where('uuid', $uuid)->first();

        if (!$order)
            return $this->respondWithError('Order not found', 404);

        $statuses = array_keys(Order::status);

        $this->validate($request, [
            'status' => 'required|in:'. implode(",", $statuses)
        ]);

        if ($order->status == ($status = $request->input('status')))
            return $this->respondWithError('This is already the status of this order', 400);

        $order->update(['status' => $status]);

        return $this->respondWithSuccess([
            'message' => 'Status updated successfully',
            'data' => new OrderResource($order)
        ]);
    }

    public function search(Request $request)
    {
        $q = rawurldecode($request->get('q', null));
        $perPage = $request->get('perPage', 12);

        if (strlen($q) < 3)
            return $this->respondWithError('Search term should be more than 3 characters', 400);

        $query = Order::latest()->where('order_number', 'like', "$q%")
            ->orWhereHas('user', function ($query) use ($q) {
                $query->where('first_name', 'like', "%$q%")
                    ->orWhere('last_name', 'like', "%$q%");
            });

        return OrderResource::collection($query->paginate($perPage));
    }

    public function test()
    {
        $order = Order::first();

//        SendSmsJob::dispatch('You have a new order', '08129672537');
//        event(new NewOrder($order));
        Notification::route('mail', 'obazeez0574@gmail.com')
            ->notify(new \App\Notifications\NewOrder($order));
    }
}
