<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Admin\Announcements;

use App\Announcement;
use App\Http\Controllers\Controller;
use App\Http\Resources\AnnouncementResource;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add(Request $request)
    {
        $this->validate($request, [
            'text' => 'required',
            'for' => 'required|in:all,agents'
        ]);

        $data = [
            'text' => $request->input('text'),
            'for' => $request->input('for')
        ];

        $announcement = Announcement::create($data);

        if (!$announcement)
            return $this->respondWithError('Announcement could not be created', 500);

        return $this->jsonResponse([
                'message' => 'Announcement created successfully',
                'data' => $announcement
            ], 201);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function list(Request $request)
    {
        $perPage = $request->get('perPage', 12);

        $announcements = Announcement::latest()->paginate($perPage);

        return AnnouncementResource::collection($announcements);
    }

    /**
     * @param string $uuid
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function details(string $uuid)
    {
        $announcement = Announcement::where('uuid', $uuid)->first();

        if (!$announcement)
            return $this->respondWithError('Announcement not found', 404);

        return $this->respondWithSuccess(['data' => new AnnouncementResource($announcement)]);
    }

    public function toggle($uuid)
    {
        $announcement = Announcement::where('uuid', $uuid)->first();

        if (!$announcement)
            return $this->respondWithError('Announcement not found', 404);

        $announcement->update(['active' => !$announcement->active]);

        return $this->respondWithSuccess('Status changed successfully');
    }

    /**
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function delete($uuid)
    {
        $announcement = Announcement::where('uuid', $uuid)->first();

        if (!$announcement)
            return $this->respondWithError('Announcement not found', 404);

        $announcement->delete();

        return $this->respondWithSuccess('Announcement deleted successfully');
    }

    /**
     * @param Request $request
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $uuid)
    {
        $announcement = Announcement::where('uuid', $uuid)->first();

        if (!$announcement)
            return $this->respondWithError('Announcement not found', 404);

        $this->validate($request, [
            'text' => 'required',
            'for' => 'required|in:all,agents'
        ]);

        $data = [
            'text' => $request->input('text'),
            'for' => $request->input('for')
        ];

        $announcement->update($data);

        return $this->respondWithSuccess([
            'message' => 'Announcement updated successfully',
            'data' => new AnnouncementResource($announcement)
        ]);
    }
}
