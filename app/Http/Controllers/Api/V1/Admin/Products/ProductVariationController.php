<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Admin\Products;

use App\Http\Controllers\Controller;
use App\Product;
use App\ProductVariation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductVariationController extends Controller
{
    public function delete(string $uuid)
    {
        $variant = ProductVariation::where('uuid', $uuid)->first();

        if (!$variant) return $this->respondWithError('Variant not found', 404);

        $variant->delete();

        return $this->respondWithSuccess('Variant deleted successfully');
    }

    public function update(Request $request, $uuid)
    {
        $variant = ProductVariation::where('uuid', $uuid)->first();

        if (!$variant) return $this->respondWithError('Variant not found', 404);

        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'agent_price' => 'required',
            'duration' => 'required'
        ]);

        $variant->update([
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'agent_price' => $request->input('agent_price'),
            'duration' => $request->input('duration')
        ]);

        return $this->respondWithSuccess('Variation updated successfully');
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'variants' => 'required|array',
            'product' => 'required|exists:products,uuid'
        ]);

        $product = Product::where('uuid', $request->input('product'))->first();

        $variants = $request->input('variants');
        DB::beginTransaction();
        array_walk($variants, function ($variant) use ($product) {
            $variant = (object)$variant;
            $data = [
                'name' => $variant->name,
                'price' => $variant->price,
                'agent_price' => $variant->agent_price,
                'duration' => $variant->duration,
                'product_id' => $product->id,
                'active' => true
            ];

            ProductVariation::create($data);
        });

        DB::commit();

        return $this->jsonResponse([
            'message' => 'Variants added successfully',
        ], 201);
    }
}
