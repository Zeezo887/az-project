<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Admin\Products;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Product;
use App\ProductVariation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $perPage = $request->get('perPage', 30);

        $products = Product::latest()->paginate($perPage);

        return ProductResource::collection($products);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'category' => 'required|exists:categories,uuid',
            'description' => 'nullable',
            'active' => 'nullable',
            'has_variant' => 'required',
            'variants' => 'nullable|array'
        ]);

        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'active' => $request->input('active', false),
            'has_variant' => $hasVariant =$request->input('has_variant', false),
            'category_id' => Category::where('uuid', $request->input('category'))->first()->id
        ];

        DB::beginTransaction();

        $product = Product::create($data);

        if ($hasVariant) {
            $variants = $request->input('variants');
            array_walk($variants, function ($variant) use ($product) {
                $variant = (object) $variant;
                $data = [
                    'name' => $variant->name,
                    'price' => $variant->price,
                    'agent_price' => $variant->agent_price,
                    'duration' => $variant->duration,
                    'product_id' => $product->id,
                    'active' => true
                ];

                ProductVariation::create($data);
            });
        }

        DB::commit();

        return $this->jsonResponse([
            'message' => 'Product Created Successfully',
            'data' => new ProductResource($product)
        ], 201);
    }

    /**
     * @param string $uuid
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function details(string $uuid)
    {
        $product = Product::where('uuid', $uuid)->first();

        if (!$product) return $this->respondWithError('Product not found', 404);

        return $this->respondWithSuccess([
            'data' => new ProductResource($product->load('variants'))
        ]);
    }

    /**
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function delete($uuid)
    {
        $product = Product::where('uuid', $uuid)->first();

        if (!$product) return $this->respondWithError('Product not found', 404);

        $product->delete();

        return $this->respondWithSuccess('Product deleted successfully');
    }

    public function search(Request $request)
    {
        $q = rawurldecode($request->get('q', null));
        $perPage = $request->get('perPage', 12);

        if (strlen($q) < 3)
            return $this->respondWithError('Search term should be more than 3 characters', 400);

        $query = Product::latest()->where('name', 'like', "%$q%");

        return ProductResource::collection($query->paginate($perPage));
    }
}
