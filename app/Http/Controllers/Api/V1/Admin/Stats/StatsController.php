<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Admin\Stats;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Support\Facades\DB;

class StatsController extends Controller
{
    public function index()
    {
        return $this->respondWithSuccess([
            'total_users' => DB::table('users')->where('user_type_id', '<>', 1)->count(),
            'total_agents' => DB::table('users')->where('user_type_id', 2)->count(),
            'total_customers' => DB::table('users')->where('user_type_id', 3)->count(),
            'total_orders' => DB::table('orders')->count(),
            'completed_orders' => DB::table('orders')->where('status', Order::STATUS_COMPLETED)->count(),
            'pending_orders' => DB::table('orders')->where('status', '<', Order::STATUS_COMPLETED)->count(),
            'cancelled_orders' => DB::table('orders')->where('status', Order::STATUS_CANCELLED)->count(),
            'product_count' => DB::table('products')->count()
        ]);
    }
}
