<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Admin\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\UserType;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        // Check if email exists
        $user = User::where('user_type_id', UserType::TYPE_ADMIN)
            ->where('email', $request->input('email'))
            ->first();

        if (!$user)
            return $this->respondWithError('User not found', 404);

        // Check if password matches
        if (!password_verify($request->input('password'), $user->password))
            return $this->respondWithError('Incorrect Password', 400);

        // generate token
        $token = JWTAuth::fromUser($user);

        return $this->jsonResponse([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL(),
            'user' => $user
        ], 200);
    }
}
