<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api\V1\Admin\Transactions;

use App\Http\Controllers\Controller;
use App\Http\Resources\TransactionResource;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        $perPage = $request->get('perPage', 30);

        $tranx = Transaction::latest()->paginate($perPage);

        return TransactionResource::collection($tranx);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'amount' => 'required',
            'user' => 'required|exists:users,uuid',
            'status' => 'required',
            'type' => 'required'
        ]);

        $data = [
            'amount' => $request->input('amount'),
            'user_id' => User::where('uuid', $request->input('user'))->first()->id,
            'status' => $request->input('status'),
            'type' => $request->input('type')
        ];

        $tranx = Transaction::create($data);

        return response()->json([
            'message' => 'Transaction saved successfully',
            'data' => $tranx
        ]);
    }

    public function search(Request $request)
    {
        $q = rawurldecode($request->get('q', null));
        $perPage = $request->get('perPage', 12);

        if (strlen($q) < 3)
            return $this->respondWithError('Search term should be more than 3 characters', 400);

        $query = Transaction::latest()
            ->where('reference', 'like', "$q%")
            ->orWhereHas('user', function ($query) use ($q) {
                $query->where('first_name', 'like', "%$q%")
                    ->orWhere('last_name', 'like', "%$q%");
            });

        return TransactionResource::collection($query->paginate($perPage));
    }
}
