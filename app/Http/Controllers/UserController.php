<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    public function profile(Request $request)
    {
        $user = $request->user();

        return view('users.index', compact('user'));
    }

    public function update(Request $request)
    {
        $me = $request->user();

        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|unique:users,phone,' . $me->id,
            'password' => 'required'
        ]);

        // Check if password is correct
        if (!Hash::check($request->input('password'), $me->password))
            return back()->with('password_error', 'Incorrect password');

        $me->update($request->only(['first_name', 'last_name', 'phone']));

        return back()->with('success', 'Profile updated successfully');
    }
}
