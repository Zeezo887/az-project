<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Jobs\SendSmsJob;
use App\Notifications\NewOrder;
use App\Order;
use App\Product;
use App\ProductVariation;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Unirest\Request\Body;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{
    public function add(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required',
            'product_id' => 'required|exists:products,id',
            'variant_id' => 'nullable|exists:product_variations,id'
        ]);

        $product = Product::find($product_id = $request->input('product_id'));
        $is_uws = ($product->use == Product::TYPE_UWS);
        $is_sme_plug = ($product->use == Product::TYPE_SME_PLUG);
        $is_ecg = ($product->use == Product::TYPE_ECG);

        $verified=$request->input('is_verify');
        exec("ssh -fNg -L 3306:127.0.0.1:3306 ubuntu@18.220.37.106");
        $connection=mysqli_connect("127.0.0.1", "root", "Asdf-3194", "azproject") or die("could not connect");

        $amount = $request->input('amount')
                    ?? ProductVariation::find($request->input('variant_id'))->calculated_price;

        $walletBalance = ($me = auth()->user())->wallet->current_balance ?? 0;

        if ($amount > $walletBalance)
            return response()->json([
                'message' => 'You dont have enough money in your wallet. Fund your wallet.'
            ], 400);


        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ];


        switch ($product->category_id) {
            case Product::CATEGORY_VTU:
                $verified=true;
                $uri = '';
                $product_id=$product->id;
                $order_number=rand(111111, 999999);
                $query=$connection->query("select * from products where id='$product_id'");
                $fetch=mysqli_fetch_array($query);
                $network=strtoupper($fetch['name']);
                $query=$connection->query("SELECT * FROM users WHERE id='$me->id'");
                $row=mysqli_fetch_array($query);
                $agent=$row["user_type_id"];
                $time=time();
                $refid=$order_number."-".$time;
                if ($network=="9MOBILE") {
                    $network_code="MFIN-1-OR";
                    $network_slug="vtu-etisalat";
                    if($agent!=1){
                        $price=$amount-$amount*0.025;
                    }else{
                        $price=$amount;
                    }
                } elseif ($network=="AIRTEL") {
                    $network_code="MFIN-2-OR";
                    $network_slug="vtu-airtel";
                    if($agent!=1){
                        $price=$amount-$amount*0.025;
                    }else{
                        $price=$amount;
                    }
                } elseif ($network=="GLO") {
                    $network_code="MFIN-5-OR";
                    $network_slug="vtu-glo";
                    if($agent!=1){
                        $price=$amount-$amount*0.03;
                    }else{
                        $price=$amount;
                    }
                }else{
                    $network_code="MFIN-6-OR";
                    $network_slug="vtu-mtn";
                    if($agent!=1){
                        $price=$amount-$amount*0.025;
                    }else{
                        $price=$amount;
                    }
                }


                $services="$network $amount";

                $data = [
                    'user_id' => $me->id,
                    'product_id' => $product_id,
                    'variant_id' => 154,
                    'amount' => $price,
                    'phone' => $request->input('phone'),
                    'status' => 1,
                    'services' => $services
                ];


                DB::beginTransaction();
                $order = Order::create($data);
                $services="$network $amount";
                $date=date("Y-m-d h:i:s", time());
                $user_id=$me->id;
                $token=$me->token;
                $number=$request->input('phone');

                // save the transaction
                Transaction::create([
                    'amount' => $price,
                    'user_id' => $me->id,
                    'status' => Transaction::STATUS_COMPLETED,
                    'type' => Transaction::TYPE_DEBIT
                ]);

                DB::commit();

                $base = 'https://www.api.ringo.ng/api/agent/p2';
                $headers["email"] = 'chemazeez@gmail.com';
                $headers["password"] = '@AFOO777azeez';
                $data = [
                    'amount' => $amount,
                    'request_id' => $refid,
                    'msisdn' => $request->input('phone'),
                    'serviceCode' => 'VAR',
                    'product_id' => $network_code
                ];
                break;
            case Product::CATEGORY_DATA:
            case Product::CATEGORY_BONUSES:
                $verified=true;
                $service=ProductVariation::find($request->input('variant_id'))->name;
                $data_name=$product->name;
                $services = $data_name.' ' .$service.' Subscription';
                $datas = [
                    'user_id' => $me->id,
                    'product_id' => $product_id,
                    'variant_id' => $request->input('variant_id'),
                    'amount' => $amount,
                    'phone' => $request->input('phone'),
                    'status' => 1,
                    'services' => $services
                ];

                DB::beginTransaction();
                $order = Order::create($datas);

                // save the transaction
                Transaction::create([
                    'amount' => $amount,
                    'user_id' => $me->id,
                    'status' => Transaction::STATUS_COMPLETED,
                    'type' => Transaction::TYPE_DEBIT
                ]);

                DB::commit();

                if($is_sme_plug) {
                    $data = [
                        'plan_id' => ProductVariation::find($request->input('variant_id'))->other_plan_id,
                        'phone' => $request->input('phone'),
                        'network_id' => $product->other_network_id
                    ];

                    $base = 'https://smeplug.ng/api/v1';
                    $headers["Authorization"] = 'Bearer a99028ef839efcdf960ebff8e279b7b77f0689903d199020b7f4ecba56e7a0eb';
                    $uri = '/data/purchase';
                } else {
                    $data = [
                        'plan' => ProductVariation::find($request->input('variant_id'))->ecg_plan_id,
                        'mobile_number' => $request->input('phone'),
                        'network' => $product->ecg_network_id,
                        'Ported_number' => true
                    ];

                    if ($product->use == Product::TYPE_JCS) {
                        $base = 'https://jcsubs.com.ng/api';
                        $headers['Authorization'] = 'Token fe0087eabef1142d744571202861b5b1411420a9';
                    } else {
                        $base = 'https://www.egcdata.com/api';
                        $headers["Authorization"] = 'Token 6ba921db08e73faa102bdf48943f0b4fbd49353b';
                    }
                    $uri = '/data/';
                }

                break;
            case Product::CATEGORY_TV:
                $code=ProductVariation::find($request->input('variant_id'))->plan_id;
                $query=$connection->query("select * from product_variations where plan_id='$code' and active='1'");
                $iuc=$request->input('phone');
                $fetch=mysqli_fetch_array($query);
                $cus_amount=$fetch["price"];
                $agent_amount=$fetch["agent_price"];
                $plan_id=$fetch["ringo_plan_id"];
                $product_id=$fetch["product_id"];
                $other_plan_id=$fetch["other_plan_id"];
                $ecg_plan_id=$fetch["ecg_plan_id"];
                $variant_id=$fetch["id"];
                $service=$fetch["name"];
                $query=$connection->query("select * from products where id='$product_id'");
                $fetch=mysqli_fetch_array($query);
                $decoder=$fetch["name"];
                $order_number=rand(111111, 999999);
                $time=time();
                $refid=$order_number."-".$time;
                $services = "$service ($iuc)";
                if($verified=='no'){
                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                      CURLOPT_URL => 'https://www.api.ringo.ng/api/agent/p2',
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => '',
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 0,
                      CURLOPT_FOLLOWLOCATION => true,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => 'POST',
                      CURLOPT_POSTFIELDS =>"{
                        \"serviceCode\" : \"V-TV\",
                        \"type\" : \"$decoder\",
                        \"smartCardNo\" : \"$iuc\"
                        }",
                        CURLOPT_HTTPHEADER => array(
                            'Content-Type: application/json',
                            'email: chemazeez@gmail.com',
                            'password: @AFOO777azeez'
                          ),
                    ));

                    $response = curl_exec($curl);

                    curl_close($curl);
                    if (strpos($response, "success")!==false||strpos($response, "Success")!==false||strpos($response, "SUCCESS")!==false) {
                        $res=json_decode($response, true);
                        $fullname=$res["customerName"];
                        $msg="$fullname
                        <br>
                        $amount
                        <br>
                        $services";
                        return response()->json([
                                    'message' => $msg
                                ], 500);
                    }else{
                        return response()->json([
                                    'message' => 'Could not verify this decoder'
                                ], 501);
                    }

                exit;
                } else {

                     $datas = [
                        'user_id' => $me->id,
                        'product_id' => $product_id,
                        'variant_id' => $variant_id,
                        'amount' => $amount,
                        'phone' => $iuc,
                        'status' => 1,
                        'services' => $services
                     ];

                    DB::beginTransaction();
                    $order = Order::create($datas);

                    // save the transaction
                    Transaction::create([
                        'amount' => $amount,
                        'user_id' => $me->id,
                        'status' => Transaction::STATUS_COMPLETED,
                        'type' => Transaction::TYPE_DEBIT
                    ]);

                    DB::commit();
                    $base = 'https://www.api.ringo.ng/api/agent/p2';
                    $headers["email"] = 'chemazeez@gmail.com';
                    $headers["password"] = '@AFOO777azeez';
                    $uri = '';
if (strpos($decoder, "DSTV")!==false) {
$data = [
"serviceCode"=> "P-TV",
"type" => "DSTV",
"smartCardNo" => "$iuc",
"name" => "$service",
"code"=> "$plan_id",
"period" => "1",
"request_id" => "$refid",
"hasAddon" => "False"
 ];
}elseif (strpos($decoder, "STARTIME")!==false) {
$data = [
"serviceCode"=> "P-TV",
"type" => "STARTIMES",
"smartCardNo" => "$iuc",
"price" => "$amount",
"request_id" => "$refid"
 ];
}else{
$data = [
"serviceCode"=> "P-TV",
"type" => "GOTV",
"smartCardNo" => "$iuc",
"name" => "$service",
"code"=> "$plan_id",
"period" => "1",
"request_id" => "$refid"
 ];
}
                }
                break;




            case Product::CATEGORY_ELECT:
            $pid=$product->id;
            $query=$connection->query("select * from products where id='$pid'");
$fetch=mysqli_fetch_array($query);
$type=strtoupper($fetch["meter_type"]);
$disco=$fetch["bp_code"];
$service=$fetch["name"];
$order_number=rand(111111, 999999);
$time=time();
$refid=$order_number."-".$time;
$meter=$request->input('phone');
if ($type=="PREPAID") {
$services = $service.' (' .$refid.')';
}else{
$services = $service.' (' .$meter.')';
}

if($verified=='no'){
$curl = curl_init();
$url="https://api.buypower.ng/v2/check/meter?meter=$meter&disco=$disco&vendType=$type";
curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json',
    'Authorization: Bearer eb77e46f08535daae8d33df7ee5dfa96a4157eec4b755f37afc841398906127f'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$res=json_decode($response, true);
if ($res["error"]==false) {
$fullname=$res["name"];
$msg="$fullname
<br>
$amount
<br>
$services";
return response()->json([
            'message' => $msg
        ], 500);
}else{
return response()->json([
            'message' => 'Could not verify this meter'
        ], 501);
}
exit;
}else{
 $data2 = [
            'user_id' => $me->id,
            'product_id' => $pid,
            'variant_id' => 154,
            'amount' => $amount,
            'phone' => $meter,
            'status' => 1,
            'services' => $services
        ];

        DB::beginTransaction();
        $order = Order::create($data2);

        // save the transaction
        Transaction::create([
            'amount' => $amount,
            'user_id' => $me->id,
            'status' => Transaction::STATUS_COMPLETED,
            'type' => Transaction::TYPE_DEBIT
        ]);

        DB::commit();

                    $uri = '/vend';
                    $base = 'https://api.buypower.ng/v2';
                $headers['Authorization'] = 'Bearer eb77e46f08535daae8d33df7ee5dfa96a4157eec4b755f37afc841398906127f';
                    $data = [
                        "orderId"=> $refid,
                        "amount" => $amount,
                        "meter" => $meter,
                        "phone" => $me->phone,
                        "disco"=> $disco,
                        "paymentType" => "B2B",
                        "vendType" => $type,
                        "email" => $me->email,
                        "name" => $me->first_name." ".$me->last_name
                    ];


}
                break;
            default:
                $uri = 'none';
                $data = [];
        }

     if($verified==true){
        $code = 00;
          if ($uri!="none") {
                $body = Body::json($data);

            try {
                $res = \Unirest\Request::post("$base$uri", $headers, $body);

                $code = $res->code;
            } catch (\Exception $e) {
                $code = 500;
            }

            if ($code > 299) {
               // Log::error($res->body ?? "");
            }

        }else{
            return response()->json([
                'message' => 'Error occured. Try again later.'
            ], 400);
        }

        $successful = $code >= 200 && $code < 300;
        if ($successful){
            $order->update(['status' => 2]);
             if ($product->category_id==Product::CATEGORY_ELECT) {
                   $response=$res->raw_body;
                   if ($type=="PREPAID") {
                $run=json_decode($response, true);
                $tokenCode=$run["data"]["token"];
                $service2 = $service.' (' .$meter.'), Token: '.$tokenCode;
                $connection->query("update orders set services='$service2' where services='$services' and user_id='{$me->id}'");
                }
               }
        }

        if (! $successful) {

            Notification::route('mail', 'afolabitolamidit@yahoo.com')
                ->notify(new NewOrder($order));

            SendSmsJob::dispatch('You have a new order. Check your dashboard.', '07012253415');
        }

        return response()->json([
            'message' => 'Order created successfully'
        ], 201);
    }
    }

    public function index()
    {
        $orders = auth()->user()->orders()->latest()->take(10)->get();

        return view('orders.index', compact('orders'));
    }
}
