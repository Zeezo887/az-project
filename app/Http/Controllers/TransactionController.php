<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Repositories\TransactionRepository;
use Illuminate\Http\Request;

/**
 * Class TransactionController
 * @package App\Http\Controllers
 */
class TransactionController extends Controller
{
    /**
     * @var TransactionRepository
     */
    private $transactionRepo;

    /**
     * TransactionController constructor.
     * @param TransactionRepository $trans
     */
    public function __construct(TransactionRepository $trans)
    {
        $this->transactionRepo = $trans;
    }

    public function toggleStatus(Request $request)
    {
        $this->validate($request, [
            'reference' => 'required|exists:transactions,reference',
            'status' => 'integer'
        ]);

        $reference = $request->input('reference');

        $trans = $this->transactionRepo->findOneBy('reference', $reference);

        $trans->update(['status' => $request->input('status')]);

        return response()->json(['message' => 'Transaction updated successfully']);
    }

}
