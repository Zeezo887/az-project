<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Repositories\CategoryRepository;
use App\Repositories\ProductsRepository;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $catRepo;

    /**
     * @var ProductsRepository
     */
    private $productRepo;

    /**
     * HomeController constructor.
     * @param CategoryRepository $catRepo
     */
    public function __construct(CategoryRepository $catRepo, ProductsRepository $productRepo)
    {
        $this->catRepo = $catRepo;
        $this->productRepo = $productRepo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function products(string $slug)
    {
        $product = $this->productRepo->findOneBy('slug', $slug);

        return view('products.index', compact('product'));
    }
}
