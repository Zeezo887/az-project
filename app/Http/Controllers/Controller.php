<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Return JsonResponse
     * @param $data
     * @param int $status_code
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonResponse($data, $status_code = 200)
    {
        return response()->json(array_merge($data, ['status_code' => $status_code,
            'code'=>($status_code == 200 || $status_code == 201) ? 'OK' : 'FAILED']), $status_code);
    }

    /**
     * Some operation (save only?) has completed successfully
     * @param mixed $data
     * @return mixed
     */
    public function respondWithSuccess($data)
    {
        return $this->jsonResponse(is_array($data) ? $data : ['message' => $data]);
    }

    /**
     * Respond with an Error
     *
     * @param string $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithError($data = 'There was an error', $code = 400)
    {
        return $this->jsonResponse(is_array($data) ? $data : ['message' => $data], $code);
    }
}
