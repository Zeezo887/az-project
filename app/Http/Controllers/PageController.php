<?php
declare(strict_types=1);

namespace App\Http\Controllers;


class PageController extends Controller
{
    public function index($page)
    {
        return view("pages.$page");
    }
}
