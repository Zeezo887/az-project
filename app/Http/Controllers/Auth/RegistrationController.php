<?php
declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\User;
use App\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class RegistrationController
 * @package App\Http\Controllers\Auth
 */
class RegistrationController extends Controller
{
    /**
     * @var UserRepository
     */
    private $usersRepo;

    /**
     * RegistrationController constructor.
     * @param UserRepository $usersRepo
     */
    public function __construct(UserRepository $usersRepo)
    {
        $this->usersRepo = $usersRepo;
    }

    /**
     * Shoe registration form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showForm()
    {
        return view('auth.register');
    }

    /**
     * Register a user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|min:11|numeric|unique:users,phone',
            'password' => 'required|min:8|confirmed',
            'user_type' => 'nullable'
        ]);

        $data = $request->except(['password', 'user_type']);
        $data['user_type_id'] = ($isAgent = $request->has('user_type')) ? UserType::TYPE_AGENT : UserType::TYPE_CUSTOMER;
        $data['status'] = $isAgent ? User::STATUS_PENDING : User::STATUS_ACTIVE;
        $data['password'] = Hash::make($request->input('password'));

        $this->usersRepo->create($data);

        return back()->with('success', 'Account created successfully');
    }
}
