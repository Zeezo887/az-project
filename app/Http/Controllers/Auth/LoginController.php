<?php
declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showForm()
    {
        return view('auth.login');
    }
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $user = User::where('email', $request->input('email'))
            ->whereIn('user_type_id', [UserType::TYPE_AGENT, UserType::TYPE_CUSTOMER])
            ->first();

        if (!$user)
            return back()->with('error', 'Invalid Email/Password');

        if ($user->status == User::STATUS_PENDING)
            return back()->with('error', 'Your account is awaiting approval');

        if ($user->status == User::STATUS_BLOCKED)
            return back()->with('error', 'Your account is blocked');

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {

            // Authentication passed...
            return redirect()->intended('/');
        }

        return back()->with('error', 'Invalid Email/Password');
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }
}
