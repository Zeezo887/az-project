<?php
declare(strict_types=1);

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Notifications\PasswordReset;
use App\ResetToken;
use App\User;
use App\UserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{
    public function change(Request $request)
    {
        $me = $request->user();

        $this->validate($request, [
            'current_password' => 'required',
            'password' => 'required|min:8|confirmed'
        ]);

        // Check current Password
        if (!Hash::check($request->input('current_password'), $me->password))
            return back()->with('change_password_error', 'Incorrect password');

        $password = Hash::make($request->input('password'));

        $me->update(['password' => $password]);

        return back()->with('success', 'Password Changed Successfully');
    }

    public function showForm()
    {
        return view('passwords.forgot');
    }

    public function requestReset(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|exists:users,email'
        ], [
            'email.exists' => 'User not found'
        ]);

        $user = User::where('email', $request->input('email'))
            ->whereIn('user_type_id', [UserType::TYPE_AGENT, UserType::TYPE_CUSTOMER])
            ->first();

        if (!$user)
            return back()->with('error', 'User not found');

        $token = $this->createToken($user);

        if (!$token)
            return back()->with('error', 'Reset link could not be sent, Try Again');

        $user->notify(new PasswordReset($token, env('APP_URL') . '/password/reset'));

        return back()->with('success', 'Reset link has been sent to your email successfully');
    }

    /**
     * @param User $user
     * @return bool|mixed
     */
    private function createToken(User $user)
    {
        // Delete previous token
        $this->clearToken($user);

        $data['token'] = bin2hex(openssl_random_pseudo_bytes(91));
        $data['expires'] = strtotime('+1 hour');

        if (!$token_data = $user->token()->create($data))
            return false;

        return $token_data->token;
    }

    /**
     * Clear previous user Tokens
     *
     * @param $user
     */
    private function clearToken($user)
    {
        $token = $user->token;

        if (!$token)
            return;

        $token->delete();
    }

    public function resetForm($token)
    {
        return view('passwords.reset', compact('token'));
    }

    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'token' => 'required|exists:reset_tokens,token',
            'password' => 'required|min:8|confirmed'
        ]);


        $token = $request->input('token');

        $reset = ResetToken::where('token', $token)->first();

        // Check if token has not expired
        if (time() >= $reset->expires) //Token has expired
            return back()->with('error', 'Token has Expired');

        $user = $reset->user;
        $user->password = Hash::make($request->input('password'));

        $user->save();

        $reset->delete();

        return back()->with('success', 'Password changed successfully');
    }

}
