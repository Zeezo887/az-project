<?php


namespace App\Http\Traits;


use App\Http\Services\Monnify;
use App\User;
use Exception;

trait BankTraits
{
    /**
     * @param User $user
     * @throws Exception
     */
    public static function createBank(User $user)
    {
        try {
            $data = (new Monnify())->createAccount($user);
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }

        $bankDetails = $data->accounts[0];
        $user->bank()->create([
            'bank_name' => $bankDetails->bankName,
            'account_name' => $bankDetails->accountName,
            'account_no' => $bankDetails->accountNumber
        ]);
    }
}
