<?php
declare(strict_types=1);

namespace App\Http\Traits;


use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Throwable;

trait ImageUploaderTrait
{
    /**
     * Upload Base 64 Image
     * @param string $path
     * @param string $fileName
     * @param string $imageData
     * @return array
     * @throws Exception
     */
//    static function uploadBase64Image(string $path, string $fileName, string $imageData)
//    {
//        $image_data = base64_decode($imageData);
//        // If the image is not a valid base 64 string
//        if (!$image_data)
//            throw new Exception("Please upload a valid image to continue.", 400);
//
//        try {
//            $extension = static::getImageMimeType($image_data);
//            $fileName = "$fileName.$extension";
//            // Upload the image
//            Storage::disk('s3')->put($path . '/' . $fileName, $image_data, [
//                'visibility' => 'public'
//            ]);
//        } catch (Throwable $t) {
//            throw new Exception('Image upload failed due to: ' . $t->getMessage(), $t->getCode());
//        }
//
//        $fullPath = "$path/$fileName";
//
//        // Let's optimize the image using a JOB
//        OptimizeS3ImageJob::dispatch($fullPath, $path, $fileName);
//
//        return [
//            'path' => $fullPath,
//            'url' => Storage::disk('s3')->url($fullPath)
//        ];
//    }

    /**
     * Get Base 64 Image Mime type
     * @param string $imageData
     * @return int|string|null
     */
    protected static function getImageMimeType(string $imageData)
    {
        $mimeTypes = array(
            "jpeg" => "FFD8",
            "png" => "89504E470D0A1A0A",
            "gif" => "474946",
            "bmp" => "424D",
            "tiff" => "4949",
            "tiff" => "4D4D"
        );

        $image_mime = NULL;

        array_walk($mimeTypes, function ($hexBytes, $mime) use ($imageData, &$image_mime) {
            $bytes = static::getBytesFromHexString($hexBytes);
            if (substr($imageData, 0, strlen($bytes)) == $bytes) {
                $image_mime = $mime;
                return;
            }
        });

        return $image_mime;
    }

    /**
     * @param string $hexData
     * @return string
     */
    private static function getBytesFromHexString(string $hexData): string
    {
        $bytes = [];

        for ($count = 0; $count < strlen($hexData); $count += 2)
            $bytes[] = chr(hexdec(substr($hexData, $count, 2)));

        return implode($bytes);
    }

    /**
     * Upload a FormData Image
     * @param string $path
     * @param string $fileName
     * @param UploadedFile $file
     * @return array
     * @throws Exception
     */
    public static function uploadFormMedia(string $path, string $fileName, UploadedFile $file): array
    {
        if (!$file->isFile() && !$file->isValid())
            throw new Exception('Please upload a valid media to continue.', 400);

        $fileName .= '.' . ($file->getClientOriginalExtension() ?? $file->getExtension());

        try {
            $file->storeAs($path, $fileName, [
                'visibility' => 'public'
            ]);
        } catch (Throwable $t) {
            throw new Exception('Media upload failed due to: ' . $t->getMessage(), $t->getCode());
        }
        $fullPath = "$path/$fileName";

        if (!Storage::disk()->exists($fullPath))
            throw new Exception('Media file upload failed.');


        return [
            'path' => $fullPath,
            'url' => Storage::disk()->url($fullPath)
        ];
    }


    /**
     * @param $path
     */
    public static function deleteImageFromS3($path): void
    {
        dispatch(static function () use ($path) {
            Storage::disk()->delete($path);
        })->onConnection('redis');
    }
}
