<?php
declare(strict_types=1);

namespace App;

/**
 * Class UserType
 * @package App
 */
class UserType extends BaseModel
{
    const TYPE_ADMIN = 1;
    const TYPE_AGENT = 2;
    const TYPE_CUSTOMER = 3;

    const types = [
        1 => 'Admin',
        2 => 'Agent',
        3 => 'Customer'
    ];

    /**
     * @var array
     */
    protected $fillable = ['type'];
}
