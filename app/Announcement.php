<?php
declare(strict_types=1);

namespace App;

/**
 * Class Announcement
 * @package App
 */
class Announcement extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = [
        'text', 'for', 'active'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];
}
