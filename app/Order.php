<?php

namespace App;

use App\Notifications\OrderCancelled;
use App\Notifications\OrderCompleted;

/**
 * Class Order
 * @package App
 */
class Order extends BaseModel
{
    const STATUS_PENDING = 0;
    const STATUS_PROCESSING = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_CANCELLED = 9;

    const status = [
        0 => 'Pending',
        1 => 'Processing',
        2 => 'Completed',
        9 => 'Cancelled'
    ];

    protected $fillable = [
        'user_id',
        'product_id',
        'variant_id',
        'amount',
        'phone',
        'status',
        'services'
    ];

    public function getStatusReadableAttribute()
    {
        return self::status[$this->status];
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->order_number = static::generateOrderNumber();
        });

        static::created(function ($model) {
            if ($model->status == self::STATUS_COMPLETED)
                static::notifyOrderCompleted($model);
        });

        static::updated(function ($model) {
            // If order was cancelled
            if ($model->status == self::STATUS_CANCELLED && $model->getOriginal('status') == self::STATUS_PROCESSING) {
                static::refundWallet($model);
                static::notifyOrderCancelled($model);
            }

            // If order was Completed
            if ($model->status == self::STATUS_COMPLETED && $model->getOriginal('status') == self::STATUS_PROCESSING)
                static::notifyOrderCompleted($model);
        });
    }

    private static function refundWallet($model)
    {
        return Transaction::create([
            'amount' => $model->amount,
            'user_id' => $model->user_id,
            'status' => Transaction::STATUS_COMPLETED,
            'type' => Transaction::TYPE_CREDIT
        ]);
    }

    private static function notifyOrderCompleted($model)
    {
        try {
            $model->user->notify(new OrderCompleted($model));
        } catch(\Exception $e) {

        }
    }

    private static function notifyOrderCancelled($model)
    {
        try {
            $model->user->notify(new OrderCancelled($model));
        } catch(\Exception $e) {

        }
    }

    private static function generateOrderNumber()
    {
        $order_number = str_pad(rand(1, 999999), 6, '0', STR_PAD_LEFT);

        if (Order::where('order_number', $order_number)->first())
            return static::generateOrderNumber();

        return $order_number;
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function variant()
    {
        return $this->belongsTo(ProductVariation::class, 'variant_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
