<?php
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;

abstract class BaseAuthenticableModel extends Authenticatable implements JWTSubject
{
    /**
     * Boot
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->uuid = (string)static::generateUUID();
        });
    }

    /**
     * Generate unique uuid
     * @return \Ramsey\Uuid\UuidInterface
     */
    private static function generateUUID()
    {
        $uuid = Str::orderedUuid();
        if (static::where('uuid', $uuid)->count() > 0) return static::generateUUID();
        return $uuid;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
