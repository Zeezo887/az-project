<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository
 * @package App\Repositories
 */
abstract class BaseRepository implements RepositoryInterface
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     * @param string $model
     */
    public function __construct(string $model)
    {
        $this->model = $model;
    }

    /**
     * Find By ID or UUID
     * @param $id
     * @return Model|null
     */
    public function find($id): ?Model
    {
        // TODO: Implement find() method.
    }

    /**
     * Find One By
     * @param string $key
     * @param $value
     * @return Model|null
     */
    public function findOneBy(string $key, $value): ?Model
    {
        return $this->model::where($key, $value)->first();
    }

    /**
     * Create record
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        return $this->model::create($data);
    }
}
