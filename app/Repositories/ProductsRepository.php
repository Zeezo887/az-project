<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Product;

/**
 * Class ProductsRepository
 * @package App\Repositories
 */
class ProductsRepository extends BaseRepository
{
    /**
     * ProductsRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(Product::class);
    }
}
