<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Transaction;

/**
 * Class TransactionRepository
 * @package App\Repositories
 */
class TransactionRepository extends BaseRepository
{
    public function __construct()
    {
        parent::__construct(Transaction::class);
    }
}
