<?php
declare(strict_types=1);

namespace App\Repositories;

use App\Category;

/**
 * Class CategoryRepository
 * @package App\Repositories
 */
class CategoryRepository extends BaseRepository
{
    /**
     * CategoryRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(Category::class);
    }

    /**
     * Get featured categories
     * @return mixed
     */
    public function featured()
    {
        return $this->model::has('products')->where('featured', true)->get();
    }
}
