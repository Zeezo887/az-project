<?php
declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Product
 * @package App
 */
class Product extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'description',
        'active',
        'featured',
        'has_variant',
        'category_id'
    ];

    protected $casts = [
        'active' => 'boolean',
        'is_corporate' => 'boolean',
        'has_variant' => 'boolean'
    ];

    const CATEGORY_VTU = 3;
    const CATEGORY_DATA = 1;
    const CATEGORY_TV = 2;
    const CATEGORY_ELECT = 5;
    const CATEGORY_BONUSES = 6;

    const TYPE_UWS = 1;
    const TYPE_SME_PLUG = 2;
    const TYPE_ECG = 3;
    const TYPE_JCS = 4;

    /**
     * Boot
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $cat = Category::find($model->category_id);
            $model->slug = (string)Str::slug($cat->slug . " " . $model->name);
        });
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * @return mixed
     */
    public function hasVariant()
    {
        return $this->has_variant;
    }

    /**
     * @return mixed
     */
    public function isCorporate()
    {
        return $this->is_corporate;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function variants()
    {
        return $this->hasMany(ProductVariation::class, 'product_id');
    }
}
