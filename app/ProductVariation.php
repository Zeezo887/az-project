<?php
declare(strict_types=1);

namespace App;

/**
 * Class ProductVariation
 * @package App
 */
class ProductVariation extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'agent_price',
        'duration',
        'product_id',
        'active'
    ];

    protected $appends = ['calculated_price'];

    public function getCalculatedPriceAttribute()
    {
        if (($user = auth()->user()) && $user->isAgent())
            return $this->agent_price;

        return $this->price;
    }
}
