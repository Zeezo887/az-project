@extends('layouts.master')

@section('title')
    My Wallet
@endsection

@section('content')

    <section class="wallet">
        <div class="row">
            <div class="columns large-4 small-12">
                <div class="wallet-card">
                    <span class="balance">₦ {{ $wallet->current_balance }}</span>
                    <span>Current Balance</span>
                </div>
            </div>
            <div class="columns large-4 small-12">
                <div class="wallet-card">
                    <span class="balance">₦ {{ $wallet->spent }}</span>
                    <span>Amount Spent</span>
                </div>
            </div>
            <div class="columns large-4 small-12">
                <div class="wallet-card">
                    <span class="balance">₦ {{ $wallet->earned }}</span>
                    <span>Amout Credited</span>
                </div>
            </div>
        </div>

        <div class="row m-t-50">
            <div class="columns large-12">
                <div class="buttons-box">
                    <a href={{ route('fund.wallet') }} class="button">Fund Wallet</a>
{{--                    <a href="#" class="button">Buy Data</a>--}}
{{--                    <a href="#" class="button">Buy VTU</a>--}}
                </div>

            </div>
        </div>

        <div class="row m-t-50">

            <div class="columns large-12">
                <p class="lead">Recent Transactions</p>
                <table class="hover">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($transactions as $transaction)
                        <tr>
                            <td>{{ $transaction->type_readable }}</td>
                            <td>{{ $transaction->amount }}</td>
                            <td>{{ $transaction->status_readable }}</td>
                            <td>{{ $transaction->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection
