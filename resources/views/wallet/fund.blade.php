@extends('layouts.master')

@section('title')
    Fund Wallet
@endsection

@section('content')

    <section class="wallet">
        <div class="row">
            <div class="columns large-6 small-12 large-offset-3">
                <h2>Fund Wallet</h2>

                <div style=" border-color: black; border-radius: 10px;">
                    <h5> Pay to Virtual Bank Account</h5>
{{--                    <p>Note: 1% service charge is deducted</p>--}}
                    <h6>Bank Name: {{$bank->bank_name}}</h6>
                    <h6>Account Name: {{$bank->account_name}}</h6>
                    <h6>Account Number: {{$bank->account_no}}</h6>

                    <br>
                    <br>
                    <h4>OR</h4>
                </div>


                    <form action="{{ route('fund.wallet.create') }}" method="post" id="fund-wallet">
                    @csrf
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap email">
                                <input name="amount" value="" size="40" id="amount"
                                       class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                       placeholder="Amount" type="number">
                            </span>
                        <br>
                        </label>

                        <label><span style="font-weight: bold">Payment Method</span> <br><br>
                            <span class="wpcf7-form-control-wrap email">
                                <input name="payment_method" value="monnify"
                                       class="wpcf7-form-control wpcf7-text wpcf7-email payment_method wpcf7-validates-as-required wpcf7-validates-as-email"
                                       placeholder="Amount" type="radio" checked> Monnify

                                <input name="payment_method" value="paystack"
                                       class="wpcf7-form-control wpcf7-text wpcf7-email payment_method wpcf7-validates-as-required wpcf7-validates-as-email"
                                       placeholder="Amount" type="radio"> Paystack
                            </span>
                        </label>
                    </p>

                    <p><input value="Proceed" class="wpcf7-form-control wpcf7-submit" type="submit"><span class="ajax-loader"></span>
                    </p>
                    <div class="wpcf7-response-output wpcf7-display-none"></div>

                </form>

                <div id="confirmation">
                    <h5>You are about to make payment of ₦<span class="confirmation-amount"></span></h5>
                    <h5>Transaction fee is ₦<span class="transaction-fee">2.00</span></h5>
                    <h4>Total is ₦<span class="total-fee">2.00</span></h4>
                    <div>
                        <button id="pay">Pay</button>
                        <button id="changeAmount">Change Amount</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    @include('scripts.wallet.fund')
@endsection
