@extends('layouts.master')

@section('title')
    About Us
@endsection

@section('content')
    <section class="titlebar ">
        <div class="row">
            <div class="right">
                <ul class="breadcrumbs">
                    <li><a href="/">Home</a></li>
                    <li><strong> About Us</strong></li>
                </ul>
            </div>
            <div>
                <h1 id="page-title" class="title text-left">About Us</h1>
            </div>
        </div>
    </section>
{{--    <section class="our_story">--}}
{{--        <div class="row">--}}
{{--            <div class="large-3 small-12 columns"></div>--}}
{{--            <div class="large-6 small-12 columns">--}}
{{--                <div class="wd-heading text-center  ">--}}
{{--                    <h2 style="margin:0;font-family:;font-weight:400;font-size:44px;line-height:94px;">--}}
{{--                        Our Story </h2>--}}

{{--                    <p style="font-family:;font-weight:400;">--}}
{{--                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sedolorm reminusto doeiusmod tempor incidition ulla--}}
{{--                        mco laboris nisi ut aliquip ex ea commo condorico consectetur adipiscing elitut aliquip. </p>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="large-3 small-12 columns"></div>--}}

{{--            <div class="large-4 small-12 columns">--}}
{{--                <div class="boxes small layout-1 clearfix ">--}}
{{--                    <div class="box-container clearfix ">--}}
{{--                        <div class="box-icon">--}}
{{--                            <img src="images/post-2-650x350.jpg"--}}
{{--                                 alt="icon">--}}
{{--                        </div>--}}
{{--                        <h3 class="box-title-1">Spiciatis unde omnis iste</h3>--}}
{{--                        <p class="box-body">Sed ut perspiciatis unde omnis iste natus error sit voluptaac cusantium doloremque--}}
{{--                            laudantium, totam rem aperiam per spiciatis unde omnis iste natus.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="large-4 small-12 columns">--}}
{{--                <div class="boxes small layout-1 clearfix ">--}}
{{--                    <div class="box-container clearfix ">--}}
{{--                        <div class="box-icon">--}}
{{--                            <img src="images/post-2-650x350.jpg"--}}
{{--                                 alt="icon">--}}
{{--                        </div>--}}
{{--                        <h3 class="box-title-1">Spiciatis unde omnis iste</h3>--}}
{{--                        <p class="box-body">Sed ut perspiciatis unde omnis iste natus error sit voluptaac cusantium doloremque--}}
{{--                            laudantium, totam rem aperiam per spiciatis unde omnis iste natus.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="large-4 small-12 columns">--}}
{{--                <div class="boxes small layout-1 clearfix ">--}}
{{--                    <div class="box-container clearfix ">--}}
{{--                        <div class="box-icon">--}}
{{--                            <img src="images/post-2-650x350.jpg"--}}
{{--                                 alt="icon">--}}
{{--                        </div>--}}
{{--                        <h3 class="box-title-1">Spiciatis unde omnis iste</h3>--}}
{{--                        <p class="box-body">Sed ut perspiciatis unde omnis iste natus error sit voluptaac cusantium doloremque--}}
{{--                            laudantium, totam rem aperiam per spiciatis unde omnis iste natus.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <section class="make_sense">
        <div class="row">
            <div class="large-6 small-12 columns">
                <div class="wd-heading text-left  ">


                    <h2 style="margin:0;font-family:;font-weight:400;font-size:44px;line-height:94px;">
                        About Us. </h2>

                    <p style="font-family:;font-weight:400;">
                        Exceltelecom is a telecommunication firm that operate on all kinds of recharge cards,
                        data of all network and TV subscription. It is a subsidiary part of EXCEL GLOBAL WORLD. </p>

                </div>
{{--                <button class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-square vc_btn3-style-flat vc_btn3-color-green">--}}
{{--                    See More--}}
{{--                </button>--}}
            </div>
            <div class="large-6 small-12 columns">
                <img class="vc_single_image-img " src="images/4-350x480.jpg" alt="4" title="4" height="480" width="350">
            </div>
        </div>
    </section>
    <section class="our_future">
        <div class="row">
            <div class="large-3 small-12 columns"></div>
            <div class="large-6 small-12 columns">
                <div class="wd-heading text-center  ">


                    <h2 style="margin:0;font-family:;font-weight:400;font-size:44px;line-height:94px;">
                        Our Mission. </h2>

                    <p style="font-family:;font-weight:400;">
                        Recruiting of a young talented youths on how to be self employed by investing in a
                        telecom business with a very small amount of money and still enjoying the benefits of wholesaler. </p>

                </div>
            </div>
            <div class="large-3 small-12 columns"></div>


            <div class="large-6 small-12 columns">
                <img class="vc_single_image-img " src="images/t48_front-450x370.jpg" alt="t48_front" title="t48_front"
                     height="370" width="450">
            </div>
{{--            <div class="large-6 small-12 columns">--}}
{{--                <ul class="tabs" data-tab role="tablist">--}}
{{--                    <li class="tab-title active" role="presentation"><a href="#panel2-1" role="tab" tabindex="0"--}}
{{--                                                                        aria-selected="true" aria-controls="panel2-1">Be Bold</a>--}}
{{--                    </li>--}}
{{--                    <li class="tab-title" role="presentation"><a href="#panel2-2" role="tab" tabindex="0" aria-selected="false"--}}
{{--                                                                 aria-controls="panel2-2">Be direct</a></li>--}}
{{--                    <li class="tab-title" role="presentation"><a href="#panel2-3" role="tab" tabindex="0" aria-selected="false"--}}
{{--                                                                 aria-controls="panel2-3">chose freedom</a></li>--}}
{{--                </ul>--}}
{{--                <div class="tabs-content">--}}
{{--                    <section role="tabpanel" aria-hidden="false" class="content active" id="panel2-1">--}}
{{--                        <p>--}}
{{--                            Bresaola beef pork loin doner tenderloin flank sausage turkey rump. Pastrami pork loin sausage bacon--}}
{{--                            tenderloin tongue kevin hamburger short loin landjaeger beef. Bresaola burgdoggen ribeye.--}}
{{--                            Chuck jerky pastrami capicola brisket pork landjaeger corned beef pork loin beef ham. Meatloaf pork belly--}}
{{--                            hamburger meatball ground round, fatback ham hock chicken.--}}
{{--                        </p>--}}
{{--                    </section>--}}
{{--                    <section role="tabpanel" aria-hidden="true" class="content" id="panel2-2">--}}
{{--                        <p>Bresaola beef pork loin doner tenderloin flank sausage turkey rump. Pastrami pork loin sausage bacon--}}
{{--                            tenderloin tongue kevin hamburger short loin landjaeger beef. Bresaola burgdoggen ribeye.--}}
{{--                            Chuck jerky pastrami capicola brisket pork landjaeger corned beef pork loin beef ham. Meatloaf pork belly--}}
{{--                            hamburger meatball ground round, fatback ham hock chicken.</p>--}}
{{--                    </section>--}}
{{--                    <section role="tabpanel" aria-hidden="true" class="content" id="panel2-3">--}}
{{--                        <p>Bresaola beef pork loin doner tenderloin flank sausage turkey rump. Pastrami pork loin sausage bacon--}}
{{--                            tenderloin tongue kevin hamburger short loin landjaeger beef. Bresaola burgdoggen ribeye.--}}
{{--                            Chuck jerky pastrami capicola brisket pork landjaeger corned beef pork loin beef ham. Meatloaf pork belly--}}
{{--                            hamburger meatball ground round, fatback ham hock chicken.</p>--}}
{{--                    </section>--}}
{{--                </div>--}}
{{--            </div>--}}


        </div>
    </section>
{{--    <section class="teams">--}}
{{--        <div class="row">--}}
{{--            <ul class="team-list small-block-grid-1 large-block-grid-2        medium-block-grid-2 text-center ">--}}
{{--                <li class="">--}}
{{--                    <div class="team-member-item">--}}
{{--                        <div class="team-member-picture">--}}
{{--                            <img src="images/4.jpg" class="attachment-metroblocks_team size-metroblocks_team" alt="4"--}}
{{--                                 srcset="images/4.jpg 529w, images/4-217x300.jpg 217w" sizes="(max-width: 529px) 100vw, 529px"--}}
{{--                                 height="730" width="529"></div>--}}
{{--                        <div class="team-member-name-job-title">--}}
{{--                            <h3 class="team-member-name">Daniel Christopher</h3>--}}
{{--                            <h4>CEO- Founder</h4>--}}
{{--                        </div>--}}
{{--                        <div class="team-member-desc text-left">--}}
{{--                            <p>Sed elit quam, iaculis sed semper sit amet, sollicitudin vitae nibh turi orno paner Quisque at magna eu--}}
{{--                                augue semper euismod. Fusce cusior lomio ujukil moman commodo molestie luctus. Donec mollis nulla ipsum,--}}
{{--                                vitae.</p>--}}
{{--                        </div>--}}
{{--                        <div class="team-membre-social-icons">--}}
{{--                            <ul>--}}
{{--                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>--}}
{{--                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>--}}
{{--                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--                <li class="">--}}
{{--                    <div class="team-member-item">--}}
{{--                        <div class="team-member-picture">--}}
{{--                            <img src="images/4.jpg" class="attachment-metroblocks_team size-metroblocks_team" alt="4"--}}
{{--                                 srcset="images/4.jpg 529w, images/4-217x300.jpg 217w" sizes="(max-width: 529px) 100vw, 529px"--}}
{{--                                 height="730" width="529"></div>--}}
{{--                        <div class="team-member-name-job-title">--}}
{{--                            <h3 class="team-member-name">Daniel Christopher</h3>--}}
{{--                            <h4>CEO- Founder</h4>--}}
{{--                        </div>--}}
{{--                        <div class="team-member-desc text-left">--}}
{{--                            <p>Sed elit quam, iaculis sed semper sit amet, sollicitudin vitae nibh turi orno paner Quisque at magna eu--}}
{{--                                augue semper euismod. Fusce cusior lomio ujukil moman commodo molestie luctus. Donec mollis nulla ipsum,--}}
{{--                                vitae.</p>--}}
{{--                        </div>--}}
{{--                        <div class="team-membre-social-icons">--}}
{{--                            <ul>--}}
{{--                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>--}}
{{--                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>--}}
{{--                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </div>--}}
{{--    </section>--}}
@endsection
