@extends('layouts.master')

@section('title')
    Contact Us
@endsection

@section('content')
    <section class="titlebar ">
        <div class="row">
            <div class="right">
                <ul class="breadcrumbs"><li><a href="/">Home</a></li><li><strong> Contact Us</strong></li></ul>				  </div>
            <div>
                <h1 id="page-title" class="title text-left">Contact Us</h1>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="large-6 small-12 columns our_office">
                <h2>OUR OFFICE</h2>

                <div class="large-6 small-12 columns">
                    <div>
                        <h3>Kwara</h3>
                        <p>Beside Leapson Water, Gbagba Area, Ilorin
                            </p>
                    </div>
                    <div>
                        <p>Email: afolabitolamidit@yahoo.com</p>
                        <p>phone: +234 816 944 8724</p>
                        <p>Whatsapp: <a href="https://wa.me/2348169448724" target="_blank">+234 816 944 8724</a></p>
                    </div>
                </div>
            </div>
            <div class="large-6 small-12 columns contact_us">
                <form method="post" action="{{ route('contact_us') }}">
                    @csrf
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap your-name"><input name="name" value="" size="40"
                                                                                   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                   placeholder="Your Name" type="text" required></span> </label>
                    </p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap your-email"><input name="email" value="" size="40"
                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                    placeholder="Your Email" type="email" required></span> </label>
                    </p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap your-subject"><input name="subject" value="" size="40"
                                                                                      class="wpcf7-form-control wpcf7-text"
                                                                                      placeholder="Your Subject"
                                                                                      type="text" required></span> </label></p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap your-message"><textarea name="message" cols="40" rows="10"
                                                                                         class="wpcf7-form-control wpcf7-textarea"
                                                                                         placeholder="Your Message" required></textarea></span>
                        </label></p>
                    <p><input value="Send" class="wpcf7-form-control wpcf7-submit" type="submit"><span class="ajax-loader"></span>
                    </p>
                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                </form>
            </div>
        </div>
    </section>
@endsection
