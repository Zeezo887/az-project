<script src="https://js.paystack.co/v1/inline.js"></script>
<script type="text/javascript" src="https://sdk.monnify.com/plugin/monnify.js"></script>
<script>
    (function($, window, document) {
        $(document).ready(function () {
            $.ajaxSetup({
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            $('#confirmation').hide();
            // When the form is submitted
            let form = $('#fund-wallet');
            let amount = 0;
            let totalAmount = 0;
            form.on('submit', function (e) {
                e.preventDefault();

                amount = parseFloat(form.find('#amount').val());
                const tranxFee = 0.01 * amount
                totalAmount = amount + tranxFee;
                const minAmount = 100;

                if (amount >= minAmount) {
                    form.hide();
                    $('#confirmation .confirmation-amount').text(amount);
                    $('#confirmation .transaction-fee').text(tranxFee);
                    $('#confirmation .total-fee').text(totalAmount);
                    $('#confirmation').show();
                }
            });

            // When the pay button is clicked
            const pay = $('#pay');
            pay.on('click', function () {

                const type = $('.payment_method:checked').val();
                $.ajax({
                    url: '{{ route('fund.wallet.create') }}',
                    type: 'POST',
                    data: {'amount':amount},
                    dataType: 'JSON',
                    complete: function (xhr, textStatus) {
                        if(200 == xhr.status){
                            const data = xhr.responseJSON.data;
                            const reference = data.reference;

                            if (type == 'paystack')
                                payWithPaystack(totalAmount * 100, reference)
                            else
                                payWithMonnify(totalAmount, reference)
                        }
                    },
                });
            });

            $('#changeAmount').on('click', function () {
                $('#confirmation').hide();
                form.show();

            })
        });

        function updateTransaction(ref, status) {
            $.ajax({
                url: '{{ route('transaction.update.status') }}',
                type: 'PATCH',
                data: {'reference':ref, 'status': status},
                dataType: 'JSON',
                complete: function (xhr, textStatus) {
                    window.location.replace('{{ route('my.wallet') }}');
                },
            });
        }

        function payWithPaystack(amount, reference){

            const handler = PaystackPop.setup({
                key: 'pk_live_6127e6e7898f2a7656fa501864d462c4a83e45ce',
                email: "{{ auth()->user()->email }}",
                amount: amount,
                currency: "NGN",
                ref: reference, // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
                metadata: {
                    custom_fields: [
                        {
                            display_name: "Mobile Number",
                            variable_name: "mobile_number",
                            value: "+2348012345678"
                        }
                    ]
                },
                callback: function(response){

                    // Mark the transaction as successful
                    updateTransaction(response.reference, 1);

                    // alert('success. transaction ref is ' + response.reference);
                },
                onClose: function(){
                    alert('window closed');
                }
            });
            handler.openIframe();
        }

        function payWithMonnify(amount, reference) {
            MonnifySDK.initialize({
                amount: amount,
                currency: "NGN",
                reference: reference,
                customerName: '{{ auth()->user()->first_name . ' ' . auth()->user()->last_name }}',
                customerEmail: '{{auth()->user()->email}}',
                customerMobileNumber: '{{auth()->user()->phone}}',
                apiKey: "MK_PROD_GGMAH4L26Y",
                contractCode: "676247193651",
                paymentDescription: "Fund Wallet",
                isTestMode: false,
                paymentMethods: ["CARD", "ACCOUNT_TRANSFER"],
                onComplete: function(response){
                    //Implement what happens when transaction is completed.

                    // updateTransaction(response.paymentReference, 1);
                },
                onClose: function(data){
                    //Implement what should happen when the modal is closed here
                    alert('window closed');
                }
            });
        }
    }(window.jQuery, window, document));
</script>
