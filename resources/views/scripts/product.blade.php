<script>
    (function($, window, document) {
        $(document).ready(function () {
            $.ajaxSetup({
                cache: false,
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            // If buy now is clicked
            $('.buy-now').on('click', function (e) {
                e.preventDefault();
                const parent = $(this).parents('.pricing-table');
                const error = $(this).siblings('.error');
                if (!parent.hasClass('active')) {
                    let activeOnes = $(this).parents('.products')
                        .find('.pricing-table.active');

                    activeOnes.removeClass('active');
                    activeOnes.find('.buy-now').text('Buy Now');
                    activeOnes.find('.error').text('').css('display', 'none');

                    parent.addClass('active');
                    $(this).text('Proceed');
                } else {
                    $('.loading').show();
                    const productId = $(this).data('product');
                    const phone = $(this).siblings('.input-phone').val();

                    const user = '{{ auth()->user()->id ?? null }}';
                    if (!user) {
                        error.text('You need to be logged in');
                        error.css('display', 'block');
                        $('.loading').hide();
                        return;
                    }


                    const variantId = $(this).data('variant');

                    let data = {
                        'phone':phone,
                        'product_id': productId,
                        'is_verify': 'no'
                    };

                    if (!variantId) {
                        // it must be vtu
                        const amount = parent.find('#no-variant-amount').val();
                        if (!amount || amount < 100) {
                            error.text('Enter a valid amount');
                            error.css('display', 'block');
                            return;
                        }

                        Object.assign(data, {'amount' : parseFloat(amount)})
                    } else {
                        Object.assign(data, {'variant_id': variantId})
                    }


                        $.ajax({
                        url: '{{ route('order.create') }}',
                        type: 'POST',
                        data: data,
                        dataType: 'JSON',
                        complete: function (xhr, textStatus) {
                            $('.loading').hide();
                            if (xhr.status == 501) {
                                error.text(xhr.responseJSON.message);
                                error.css('display', 'block');
                                return;
                             }else
                            if (xhr.status == 500) { 
                                xdialog.confirm(xhr.responseJSON.message, function() {
     $('.loading').show();
                            Object.assign(data, {'is_verify': 'yes'})   
                            $.ajax({
                        url: '{{ route('order.create') }}',
                        type: 'POST',
                        data: data,
                        dataType: 'JSON',
                        complete: function (xhr, textStatus) {
                            $('.loading').hide();
                            if (xhr.status !== 201) {
                                error.text(xhr.responseJSON.message);
                                error.css('display', 'block');
                                return;
                            }
                            window.location.replace('{{ route('my.orders') }}');
                        },
                    });
    console.info('Done!');
  }, {
    style: 'width:420px;font-size:0.8rem;',
    buttons: {
      ok: 'Proceed',
      cancel: 'Cancel'
    },
    oncancel: function() {
         error.text('Cancelled');
                                error.css('display', 'block');
                                return;
      console.warn('Cancelled!');
    }
  });
                          
                            }else{    
                            if (xhr.status !== 201) {
                                error.text(xhr.responseJSON.message);
                                error.css('display', 'block');
                                return;
                            }
                            window.location.replace('{{ route('my.orders') }}');
                        }
                        },
                    });

                    
                }
            });
        });
    }(window.jQuery, window, document));
</script>
<link href="https://cdn.jsdelivr.net/gh/xxjapp/xdialog@3/xdialog.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/gh/xxjapp/xdialog@3/xdialog.min.js"></script>
