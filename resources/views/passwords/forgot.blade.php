@extends('layouts.master')

@section('title')
    Forgot Password
@endsection

@section('content')
    <section class="register contact_us">
        <div class="row">
            <div class="columns large-6 small-12 large-offset-3">
                <h2>Forgot Password</h2>

                @if (session('error'))
                    <div class="my-alert error">
                        <span>{{ session('error') }}</span>
                    </div>
                @endif
                @if (session('success'))
                    <div class="my-alert success">
                        <span>{{ session('success') }}</span>
                    </div>
                @endif
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="my-alert error">
                            <span>{{ $error }}</span>
                        </div>
                    @endforeach
                @endif

                <form action="{{ route('password.request.reset') }}" method="post">
                    @csrf
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap email"><input name="email" value="{{ old('email') }}" size="40"
                                                                               class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                               placeholder="Email" type="email" required></span> </label>
                    </p>

                    <p><input value="Continue" class="wpcf7-form-control wpcf7-submit" type="submit"><span class="ajax-loader"></span>
                        <a href="{{ route('login') }}" class="float-right">I remember my password. Login</a>
                    </p>
                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                </form>
            </div>
        </div>
    </section>
@endsection
