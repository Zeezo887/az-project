@extends('layouts.master')

@section('title')
    Reset Password
@endsection

@section('content')
    <section class="register contact_us">
        <div class="row">
            <div class="columns large-6 small-12 large-offset-3">
                <h2>Reset Password</h2>

                @if (session('error'))
                    <div class="my-alert error">
                        <span>{{ session('error') }}</span>
                    </div>
                @endif
                @if (session('success'))
                    <div class="my-alert success">
                        <span>{{ session('success') }}</span>
                    </div>
                @endif
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="my-alert error">
                            <span>{{ $error }}</span>
                        </div>
                    @endforeach
                @endif

                <form action="{{ route('password.reset') }}" method="post">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap password"><input name="password" value="" size="40"
                                                                                  class="wpcf7-form-control wpcf7-text"
                                                                                  type="password" placeholder="Password" required></span> </label></p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap phone"><input name="password_confirmation" value="" size="40"
                                                                               class="wpcf7-form-control wpcf7-text"
                                                                               type="password" placeholder="Confirm Password" required></span> </label></p>


                    <p><input value="Reset" class="wpcf7-form-control wpcf7-submit" type="submit"><span class="ajax-loader"></span>
                        <a href="{{ route('login') }}" class="float-right">I remember my password. Login</a>
                    </p>
                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                </form>
            </div>
        </div>
    </section>
@endsection
