<section class="l-footer-columns">
    <h3 class="hide">Footer</h3>
    <div class="row">
        <section class="block">
            <div class="large-3 columns">
                <div>
                    <div class="textwidget"><img
                            style="display: block; background: rgba(255, 255, 255, 0.9) none repeat scroll 0% 0%; padding: 15px 20px; border-radius: 2px;"
                            src="{{ asset('images/logo.png') }}"
                            alt="consulting theme">
                        <br>
                        <p>Recruiting of a young talented youths on how to be self employed by investing in a telecom business
                            with a very small amount of money and still enjoying the benefits of wholesaler.</p>
                        <br>
                        <a class="footer-readmor" href="/contact">Get in Touch <i class="fa fa-long-arrow-right"></i></a></div>
                </div>
            </div>
            <div class="large-3 columns">
                <div><h2 class="block-title">Business Hours</h2>
                    <div class="textwidget"><h6>Opening Days :</h6>
                        <div style="margin : 0  0 10px 0">Monday – Friday : 9am to 7 pm</div>
                        <div style="margin : 0  0 30px 0">Saturday : 9am to 5 pm</div>
                        <h6>Vacations :</h6>

                        <div style="margin : 0  0 10px 0">All Sunday Days</div>
                        <div style="margin : 0  0 10px 0">All Official Holidays</div>
                    </div>
                </div>
            </div>


            <div class="large-3 columns">

                <div><h2 class="block-title">Navigate</h2>
                    <div class="textwidget">
                        <div class="menu-left-menu-container">
                            <ul id="menu-left-menu" class="menu">
                                <li id="menu-item-1993" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1993">
                                    <a href="/about">About Us</a></li>
                                <li id="menu-item-1992" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1992">
                                    <a href="/contact">Get In Touch</a></li>

                                <li id="menu-item-2370" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2370">
                                    <a href="{{ route('home') }}">Home</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</section>
<a href="https://wa.me/2348169448724" target="_blank" class="whatsapp-btn">
    <span class="shadow">
            <img src="{{ asset('/images/whatsapp.png') }}" class="img-fluid" alt=""></span>
</a>
