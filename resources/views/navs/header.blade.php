<header class="l-header creative-layout">
<b>
    <div class="sticky">
        <nav class="top-bar" data-topbar="">
            <div class="page-section home-page large-1 columns" id="page-content">
                <ul class="title-area ">

                    <li class="name">
                        <h1><a href="{{ route('home') }}" rel="home" title="Voip WordPress Theme" class="active"><img
                                    src="{{ asset('images/logo.png') }}" alt=""></a></h1>
                    </li>
                    <li class="toggle-topbar menu-icon">
                        <a href="#"><span>Menu</span></a>
                    </li>
                </ul>

                <ul class="social-icons accent inline-list right">
                </ul>

            </div>


            <section class="creative top-bar-section large-9 columns">
                <div class="menu-main-men-container">
                    <ul id="menu-main-men" class="menu right">
                        <li id="menu-item-2366" class="color-2 current-menu-item">
                            <a href="{{ route('home') }}" class="has-icon"><i class="---- None ---- fa"></i> Home</a>
                        </li>


                        @foreach($categories as $category)
                             <li id="menu-item-{{$category->slug}}" class="{{ $category->products()->count() ? 'has-dropdown' : ''}} not-click  color-2">
                                <a href="#" class="has-icon"><i class="---- None ---- fa"></i> {{ $category->name }}</a>

                                @if ($category->products()->count())
                                    <ul class="sub-menu dropdown" style="background-color: black;">
                                       
                                       <li class="title back js-generated"><h5><a href="javascript:void(0)"><font color="white">Back</font></a></h5></li>
                                        <li class="parent-link hide-for-large-up"><a class="parent-link js-generated" href="#"><i
                                                    class="---- None ---- fa"></i><font color="white"> {{ $category->name }} </font></a></li>


                                        @foreach($category->products as $product)
                                            <li id="menu-item-{{ $product->slug }}" class="color-2">
                                                <a href="{{ route('product', $product->slug) }}" class="has-icon"><i class="---- None ---- fa"></i> <font color="white">{{ $product->name }}</font></a>
                                            </li>
                                        @endforeach 
                                    </ul>
                                @endif

                            </li>
{{--                            <li id="menu-item-{{$category->uuid}}" class="color-3">--}}
{{--                                <a href="/{{ $category->slug }}" class="has-icon"><i class="---- None ---- fa"></i><font color="black"> {{ $category->name }}</font></a>--}}
{{--                            </li>--}}

                        @endforeach

                        @php
                        $userType = auth()->user()->user_type_id ?? false;

                        $notifications = [];
                        if ($userType && $userType === 2)
                            $notifications = \App\Announcement::where('active', true)->latest()->get();

                        if ($userType && $userType === 3)
                            $notifications = \App\Announcement::where('active', true)->where('for', 'all')->latest()->get();

                        @endphp

                        @if (count($notifications) && auth()->user())
                            <li id="menu-item-notifications" class="has-dropdown not-click color-2">
                                <a href="#" class="has-icon"><i class="---- None ---- fa"></i> Announcements</a>

                                <ul class="sub-menu dropdown ">
                                    <li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li>
                                    <li class="parent-link hide-for-large-up"><a class="parent-link js-generated" href="#"><i
                                                class="---- None ---- fa"></i> Announcements</a></li>


                                    @foreach($notifications as $notification)
                                        <li id="menu-item-{{ $notification->id }}" class="color-2">
                                            <a href="#!" class="has-icon"><i class="---- None ---- fa"></i> {{ $notification->text }}</a>
                                        </li>
                                    @endforeach


                                </ul>
                                {{--                                @endif--}}

                            </li>
                        @endif

{{--                        <li id="menu-item-7" class="color-4">--}}
{{--                            <a href="/about" class="has-icon"><i class="---- None ---- fa"></i> About Us</a>--}}
{{--                        </li>--}}

{{--                        <li id="menu-item-23" class="color-5">--}}
{{--                            <a href="blog.html" class="has-icon"><i class="---- None ---- fa"></i> Blog</a>--}}
{{--                        </li>--}}


{{--                        <li id="menu-item-27" class="color-8">--}}
{{--                            <a href="/contact" class="has-icon"><i class="---- None ---- fa"></i> Contact Us</a>--}}
{{--                        </li>--}}
                    </ul>
                </div>


            </section>
            <section class="creative log_menu  top-bar-section large-1 columns">
                <div class="menu-secondary-menu-container">
                    @if(auth()->user())
                        <ul id="menu-secondary-menu" class="menu right">
                            <li id="menu-item-2209" class="color-10">
                                <a href="{{ route('my.wallet') }}" class="has-icon"><i class="---- None ---- fa"></i>Balance: ₦ {{ number_format(auth()->user()->wallet->current_balance, 2) }}</a>
                            </li>

                            <li id="menu-item-2210" class="color-11">
                                <a href="{{ route('my.orders') }}" class="has-icon"><i class="---- None ---- fa"></i>Orders({{ auth()->user()->orders()->where('status', 1)->count() }})</a>
                            </li>

                            <li id="menu-item-2211" class="color-11">
                                <a href="{{ route('my.profile') }}" class="has-icon"><i class="---- None ---- fa"></i>Settings</a>
                            </li>


                            <li id="menu-item-2210" class="color-11">
                                <a href="{{ route('logout') }}" class="has-icon"><i class="---- None ---- fa"></i>Logout</a>
                            </li>

{{--                            <li id="menu-item-2208" class="has-dropdown not-click  color-2">--}}
{{--                                <a href="#!" class="has-icon"><i class="---- None ---- fa"></i> {{ auth()->user()->first_name }}</a>--}}
{{--                                <ul class="sub-menu dropdown ">--}}
{{--                                    <li class="title back js-generated"><h5><a href="javascript:void(0)">Back</a></h5></li>--}}
{{--                                    <li class="parent-link hide-for-large-up"><a class="parent-link js-generated" href="index.html"><i--}}
{{--                                                class="---- None ---- fa"></i> {{ auth()->user()->first_name }}</a></li>--}}
{{--                                    <li id="menu-item-3333" class="color-2">--}}
{{--                                        <a href="{{ route('my.profile') }}" class="has-icon"><i class="---- None ---- fa"></i> Settings</a>--}}
{{--                                    </li>--}}
{{--                                    <li id="menu-item-3334" class="color-2">--}}
{{--                                        <a href="{{ route('logout') }}" class="has-icon"><i class="---- None ---- fa"></i> Logout</a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}

                        </ul>
                    @else
                        <ul id="menu-secondary-menu" class="menu right">
                            <li id="menu-item-2211" class="color-9">
                                <a href="{{ route('login') }}" class="has-icon"><i class="---- None ---- fa"></i> Sign In</a>
                            </li>

                            <li id="menu-item-2212" class="color-10">
                                <a href="{{ route('register') }}" class="has-icon"><i class="---- None ---- fa"></i> Sign Up</a>
                            </li>
                        </ul>
                    @endif
                </div>

            </section>
{{--            <section class="large-1 columns search-section hide-for-small-only">--}}
{{--                <form action="index.html" class="searchform" id="searchform" method="get" role="search">--}}
{{--                    <div>--}}
{{--                        <input id="s" name="s" class="hide" value="" placeholder="Search..." type="text">--}}
{{--                        <a href="#"> <i class="fa fa-search" aria-hidden="true"></i></a>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--                <div class="show-cart-btn hide-for-small-only">--}}

{{--                    <div class="hidden-cart" style="display: none;">--}}
{{--                        <div class="widget woocommerce widget_shopping_cart">--}}
{{--                            <div class="widget_shopping_cart_content">--}}

{{--                                <ul class="cart_list product_list_widget ">--}}


{{--                                    <li class="empty">No products in the cart.</li>--}}


{{--                                </ul><!-- end product list -->--}}


{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </section>--}}
        </nav>
    </div>
    <!--/.top-Menu -->
</b>
</header>
