@extends('layouts.master')

@section('title')
    My Orders
@endsection

@section('content')

    <section class="wallet">

        <div class="row m-t-50">
            <div class="wd-heading text-center  ">

                <h2 style="margin:0;font-weight:400;">My Orders
                </h2>

                <hr style="border-bottom-color: #db4436; margin: 10px 0;">
            </div>
            <div class="columns large-12">
                <table class="hover">
                    <thead>
                    <tr>
                        <th>Order Number</th>
                        <th>Product</th>
                        <th>Amount</th>
                        <th>Phone</th>
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                       
                    @foreach($orders as $order)
                     <?php
                        if ($order->services!=null) {
                           $services=$order->services;
                        }else{
                            $services="{$order->product->name}";
                        }
                        ?>
                        <tr>
                            <td>#{{ $order->order_number }}</td>
                            <td>{{ $services}} </td>
                            <td>{{ $order->amount }}</td>
                            <td>{{ $order->phone }}</td>
                            <td>{{ $order->status_readable }}</td>
                            <td>{{ $order->created_at->format('M d Y H:i') }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection
