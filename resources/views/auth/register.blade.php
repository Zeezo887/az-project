@extends('layouts.master')

@section('title')
    Sign Up
@endsection

@section('content')
    <section class="register contact_us">
        <div class="row">
            <div class="columns large-6 small-12 large-offset-3">
                <h2>Create your account in seconds</h2>
                @if (session('success'))
                    <div class="my-alert success">
                        <span>{{ session('success') }} <a href="{{ route('login') }}">Proceed to Login</a></span>
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="my-error">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('register.submit') }}" method="post">
                    @csrf
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap first-name"><input name="first_name" size="40"
                                                                                    value="{{ old('first_name') }}"
                                                                                   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                   placeholder="First Name" type="text" required></span> </label>
                    </p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap last_name"><input name="last_name" value="{{ old('last_name') }}" size="40"
                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                    placeholder="Last Name" type="text" required></span> </label>
                    </p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap email"><input name="email" value="{{ old('email') }}" size="40"
                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                    placeholder="Email" type="email" required></span> </label>
                    </p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap phone"><input name="phone" value="{{ old('phone') }}" size="40"
                                                                                      class="wpcf7-form-control wpcf7-text"
                                                                                      type="text" placeholder="Phone" required></span> </label></p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap password"><input name="password" value="" size="40"
                                                                               class="wpcf7-form-control wpcf7-text"
                                                                               type="password" placeholder="Password" required></span> </label></p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap phone"><input name="password_confirmation" value="" size="40"
                                                                               class="wpcf7-form-control wpcf7-text"
                                                                               type="password" placeholder="Confirm Password" required></span> </label></p>

                    <p><label><br>
                            <span class="wpcf7-form-control-wrap phone"><input name="user_type" size="40"
                                                                               class="wpcf7-form-control "
                                                                               type="checkbox"> I am an agent (By choosing this option you are confirming that you are an agent. Your account will be reviewed by the admin before approval)</span></label></p>


                    <p><input value="Create your account" class="wpcf7-form-control wpcf7-submit" type="submit"><span class="ajax-loader"></span>
                    </p>
                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                </form>
            </div>
        </div>
    </section>
@endsection
