@extends('layouts.master')

@section('title')
    Sign In
@endsection

@section('content')
    <section class="register contact_us">
        <div class="row">
            <div class="columns large-6 small-12 large-offset-3">
                <h2>Sign In</h2>

                @if (session('error'))
                    <div class="my-alert error">
                        <span>{{ session('error') }}</span>
                    </div>
                @endif
                <form action="{{ route('login.submit') }}" method="post">
                    @csrf
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap email"><input name="email" value="{{ old('email') }}" size="40"
                                                                               class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                               placeholder="Email" type="email" required></span> </label>
                    </p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap password"><input name="password" value="" size="40"
                                                                                  class="wpcf7-form-control wpcf7-text"
                                                                                  type="password" placeholder="Password" required></span> </label></p>

                    <p><input id="checkbox3" type="checkbox"><label for="checkbox3">Remember Me</label></p>
                    <p><input value="Sign In" class="wpcf7-form-control wpcf7-submit" type="submit"><span class="ajax-loader"></span>
                        <a href="{{ route('forgot.password') }}" class="float-right">Forgot Password?</a>
                    </p>
                    <p><a href="{{ route('register') }}">Don't have an account? Sign up instead.</a></p>
                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                </form>
            </div>
        </div>
    </section>
@endsection
