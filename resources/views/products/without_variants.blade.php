<div class="columns large-offset-4 large-4 small-12">
    <div class="pricing-table with-variant product-data">
        <div class="pricing-table-info">
            <div class="title"><h2>{{ $product->name }}</h2></div>
            <div class="price"><h4>{{ $product->category->name }}</h4></div>
{{--            <div class="description"><p>{{ $variant->duration }}</p></div>--}}
            <input type="number" style="text-align: center" placeholder="Enter Amount" value="1000" id="no-variant-amount">
        </div>
        <div class="pricing-table-button">
            <i class="fa fa-arrow-down"></i>
            <div class="cta-button">
                <input type="number" placeholder="Phone Number" value="" class="input-phone">
                <a href="#" data-product="{{ $product->id }}" data-variant="{{ $variant->id ?? '' }}" class="button buy-now">Buy Now</a>
                <span class="error"></span>
            </div>
        </div>
    </div>
</div>
