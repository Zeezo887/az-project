@extends('layouts.master')

@section('title')
    {{ $product->category->name }} - {{ $product->name }}
@endsection

@section('content')
    <section class="products">
        <div class="row">
            <div class="wd-heading text-center  ">

                <h2 style="margin:0;font-family:;font-weight:400;">{{ $product->category->name }} - {{ $product->name }}
                     </h2>

                <hr style="border-bottom-color: #db4436; margin: 10px 0;">
            </div>

            @if($product->category_id==3)
            <div class="columns large-offset-4 large-4 small-12">
    <div class="pricing-table with-variant product-data">
        <div class="pricing-table-info">
            <input type="number" style="text-align: center" placeholder="Enter Amount" value="100" id="no-variant-amount">
        </div>
        <div class="pricing-table-button">
            <i class="fa fa-arrow-down"></i>
            <div class="cta-button">
                <input type="number" placeholder="Phone Number" value="" class="input-phone" name="phone">
                <a href="#" data-product="{{ $product->id }}" data-variant="{{ $variant->id ?? '' }}" class="button buy-now">Recharge Now</a>
                <span class="error"></span>
            </div>
        </div>
    </div>
</div>
            @elseif($product->category_id==5)
            <div class="columns large-offset-4 large-4 small-12">
    <div class="pricing-table with-variant product-data">
        <div class="pricing-table-info">
            <div class="title"><h2>{{ $product->name }}</h2></div>
            <div class="price"><h4>{{ $product->category->name }}</h4></div>
{{--            <div class="description"><p>{{ $variant->duration }}</p></div>--}}
            <input type="number" style="text-align: center" placeholder="Enter Amount" value="" id="no-variant-amount">
        </div>
        <div class="pricing-table-button">
            <i class="fa fa-arrow-down"></i>
            <div class="cta-button">
                <input type="number" placeholder="Meter Number" class="input-phone">
                <a href="#" data-product="{{ $product->id }}" data-variant="{{ $variant->id ?? '' }}" class="button buy-now">Buy Now</a>
                <span class="error"></span>
            </div>
        </div>
    </div>
</div>
            @elseif($product->id==23||$product->id==34||$product->id==35)
            @foreach($product->variants as $variant)
    <div class="columns large-4 small-12">
        <div class="pricing-table with-variant product-data">
            <div class="pricing-table-info">
                <div class="title"><h2>{{ $variant->name }}</h2></div>
                <div class="price"><h4>₦ {{ number_format($variant->calculated_price) }}</h4></div>
                <div class="description"><p>{{ $variant->duration }}</p></div>
            </div>
            <div class="pricing-table-button">
                <i class="fa fa-arrow-down"></i>
                <div class="cta-button">
                    <input type="number" placeholder="IUC Number" class="input-phone">
                    <a href="#" data-product="{{ $product->id }}" data-variant="{{ $variant->id }}" class="button buy-now">Buy Now</a>
                    <span class="error"></span>
                </div>
            </div>
        </div>
    </div>
@endforeach

            @elseif($product->hasVariant())
                @include('products.with_variants', ['product' => $product])
            @else
                @include('products.without_variants', ['product' => $product])
            @endif
        </div>

    </section>
@endsection

@section('scripts')
    @include('scripts.product')
@endsection
