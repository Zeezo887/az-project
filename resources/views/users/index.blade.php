@extends('layouts.master')

@section('title')
    Settings
@endsection

@section('content')

    <section class="wallet">
        <div class="row m-t-50">
            <div class="wd-heading text-center  ">

                <h2 style="margin:0;font-family:;font-weight:400;">Settings
                </h2>

                <hr style="border-bottom-color: #db4436; margin: 10px 0;">
            </div>
            @if (session('success'))
                <div class="my-alert success">
                    <span>{{ session('success') }}</span>
                </div>
            @endif

            @if (count($errors) > 0)
                <div class="my-error">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="columns large-7">
                <h4>Update Profile</h4>
                <form action="{{ route('my.profile.update') }}" method="post">
                    @csrf
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap first-name"><input name="first_name" size="40"
                                                                                    value="{{ auth()->user()->first_name }}"
                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                    placeholder="First Name" type="text" required></span> </label>
                    </p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap last_name"><input name="last_name" value="{{ auth()->user()->last_name }}" size="40"
                                                                                   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                   placeholder="Last Name" type="text" required></span> </label>
                    </p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap email"><input name="email" value="{{ auth()->user()->email }}" size="40"
                                                                               class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                               placeholder="Email" type="email" required disabled></span> </label>
                    </p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap phone"><input name="phone" value="{{ auth()->user()->phone }}" size="40"
                                                                               class="wpcf7-form-control wpcf7-text"
                                                                               type="text" placeholder="Phone" required></span> </label></p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap password"><input name="password" value="" size="40"
                                                                                  class="wpcf7-form-control wpcf7-text"
                                                                                  type="password" placeholder="Password" required></span>
                        @if(session('password_error'))
                            <span class="error">{{ session('password_error') }}</span>
                        @endif
                        </label></p>

                    <p><input value="Update Profile" class="wpcf7-form-control wpcf7-submit" type="submit"><span class="ajax-loader"></span>
                    </p>
                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                </form>

            </div>

            <div class="columns large-5">
                <h4>Change Password</h4>
                <form action="{{ route('change.password') }}" method="post">
                    @csrf
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap password"><input name="current_password" value="" size="40"
                                                                                  class="wpcf7-form-control wpcf7-text"
                                                                                  type="password" placeholder="Current Password" required></span>
                            @if(session('change_password_error'))
                                <span class="error">{{ session('change_password_error') }}</span>
                            @endif</label></p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap password"><input name="password" value="" size="40"
                                                                                  class="wpcf7-form-control wpcf7-text"
                                                                                  type="password" placeholder="New Password" required></span> </label></p>
                    <p><label><br>
                            <span class="wpcf7-form-control-wrap password"><input name="password_confirmation" value="" size="40"
                                                                                  class="wpcf7-form-control wpcf7-text"
                                                                                  type="password" placeholder="Confirm New Password" required></span> </label></p>

                    <p><input value="Change Password" class="wpcf7-form-control wpcf7-submit" type="submit"><span class="ajax-loader"></span>
                    </p>
                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                </form>
            </div>
        </div>

{{--        <div class="row m-t-50">--}}

{{--            <div class="columns large-12">--}}
{{--                <p class="lead">Recent Transactions</p>--}}
{{--                <table class="hover">--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                        <th>Type</th>--}}
{{--                        <th>Amount</th>--}}
{{--                        <th>Status</th>--}}
{{--                        <th>Date</th>--}}
{{--                    </tr>--}}
{{--                    </thead>--}}
{{--                    <tbody>--}}
{{--                    @foreach($transactions as $transaction)--}}
{{--                        <tr>--}}
{{--                            <td>{{ $transaction->type_readable }}</td>--}}
{{--                            <td>{{ $transaction->amount }}</td>--}}
{{--                            <td>{{ $transaction->status_readable }}</td>--}}
{{--                            <td>{{ $transaction->created_at }}</td>--}}
{{--                        </tr>--}}
{{--                    @endforeach--}}
{{--                    </tbody>--}}
{{--                </table>--}}
{{--            </div>--}}
{{--        </div>--}}
    </section>

@endsection
