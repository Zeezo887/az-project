<!DOCTYPE html>
<html>
<head>
<style>
table {
  border-collapse: collapse;
}

th, td {
  padding: 8px;
  text-align: left;
  border-bottom: 1px solid #DDD;
}

tr:hover {background-color: #D6EEEE;}
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}

tr:nth-child(even) {
  background-color: rgba(150, 212, 212, 0.4);
}

th:nth-child(even),td:nth-child(even) {
  background-color: rgba(150, 212, 212, 0.4);
}
</style>
</head>
<body>

<h2>EXCELTELECOM PRODUCT PLANS & CODES</h2>
<p>Please send the correct plan codes for each of our products you are purchasing for accurate billing.</p>

<?php 
include"../functions.php";
$token=sanitize($connection, $_GET["token"]);
$email=sanitize($connection, $_GET["email"]);

if(!empty($token)&&mysqli_num_rows($connection->query("select * from users where uuid='$token' and email='$email'"))){

$res2=$connection->query("SELECT * FROM product_variations where product_id='9' or product_id='10' or product_id='11' or product_id='24' ORDER BY `id` asc");
echo "
<table>
<tr>
<td>
Plan Name
</td>
<td>
Plan ID
</td>
</tr>
";
while($row2=mysqli_fetch_array($res2)){
$pid=$row2["product_id"];
$query=$connection->query("select * from products where id='$pid'");
$fetch=mysqli_fetch_array($query);
$name=$fetch["name"];
$cus_amount=$row2["price"];
$agent_amount=$row2["agent_price"];
if(getAgent($user_id)==true){
$row2["amount"]=$agent_amount;  
}else{
$row2["amount"]=$cus_amount;   
}
$code=$row2["plan_id"];
$name=$name." ".$row2["name"]; 
$plandata[]=$row2;
if ($code) {
echo"
<tr>
<td>
$name
</td>
<td>
$code
</td>
</tr>
";
 
}
}

$res22=$connection->query("SELECT * FROM product_variations where product_id='99' OR product_id='114' ORDER BY `id` asc");
while($row22=mysqli_fetch_array($res22)){
$pid=$row22["product_id"];
$query22=$connection->query("select * from products where id='$pid'");
$fetch22=mysqli_fetch_array($query22);
$name=$fetch22["name"];
$cus_amount=$row22["price"];
$agent_amount=$row22["agent_price"];
if(getAgent($user_id)==true){
$row22["amount"]=$agent_amount;  
}else{
$row22["amount"]=$cus_amount;   
}
$code=$row22["ecg_plan_id"];
$name=$name." ".$row22["name"]; 
if ($code) {
echo"
<tr>
<td>
$name
</td>
<td>
$code
</td>
</tr>
";
 
}
}

$res2=$connection->query("SELECT * FROM products where bp_code!='' and  category_id='5' ORDER BY `id` asc");
while($row2=mysqli_fetch_array($res2)){
$code=$row2["id"];
$name=$row2["name"];  
if ($code) {
echo"
<tr>
<td>
$name
</td>
<td>
$code
</td>
</tr>
";
 
}
}

$res2=$connection->query("SELECT * FROM product_variations where product_id='23' or ringo_plan_id!='' and product_id='34' or ringo_plan_id!='' and product_id='35' ORDER BY `id` asc");
while($row2=mysqli_fetch_array($res2)){
$pid=$row2["product_id"];
$query=$connection->query("select * from products where id='$pid'");
$fetch=mysqli_fetch_array($query);
$name=$fetch["name"];
$cus_amount=$row2["price"];
$agent_amount=$row2["agent_price"];
if(getAgent($user_id)==true){
$row2["amount"]=$agent_amount;  
}else{
$row2["amount"]=$cus_amount;   
}
$code=$row2["plan_id"];
$name=$name." ".$row2["name"]; 
if ($code) {
echo"
<tr>
<td>
$name
</td>
<td>
$code
</td>
</tr>
";
 
}
}



echo "
</table>
";


} else{
$obj->status="error";
$obj->message="Could not access";
$json=json_encode($obj);
echo $json;
}
?>
</body>
</html>