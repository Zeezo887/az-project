<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'https://Zeezo887@bitbucket.org/Zeezo887/az-project.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', [".env"]);
add('shared_dirs', ["storage"]);

// Writable dirs by web server
add('writable_dirs', []);


// Hosts

host('18.220.37.106')
    ->user('ubuntu')
    ->set('deploy_path', '/var/www/laravel');

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

task('artisan:route:clear', function () {
    $needsVersion = 5.6;
    $currentVersion = get('laravel_version');
    if (version_compare($currentVersion, $needsVersion, '>=')) {
        run('{{bin/php}} {{release_path}}/artisan route:clear');
    }
});

task('artisan:config:clear', function () {
    $needsVersion = 5.6;
    $currentVersion = get('laravel_version');
    if (version_compare($currentVersion, $needsVersion, '>=')) {
        run('{{bin/php}} {{release_path}}/artisan config:clear');
    }
});

task('restart:server', function () {
    run('sudo systemctl restart php7.2-fpm');
});

after('deploy:vendors', 'artisan:migrate');

after('artisan:optimize', 'artisan:route:clear');

after('artisan:route:clear', 'artisan:config:clear');

//after('artisan:config:clear', 'start:socket');
after('deploy:symlink', 'restart:server');
// Restart supervisor handling queue workers
//after('restart:server', 'restart:supervisor');
// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

